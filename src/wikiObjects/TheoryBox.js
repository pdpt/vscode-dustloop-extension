// TheoryBox.js
/* eslint-disable max-len */
/**
 * Theory Box
 * @link <https://www.dustloop.com/w/Template:TheoryBox/doc>
 * @param {string} title      A name or label for the route. Appears in bold text.
 * @param {string} oneliner   A short, one-sentence descriptor for the route. Appears in italics.
 * @param {number} damage		  Damage delt to Ky from the combo.
 * @param {string} difficulty A brief descriptor of the approximate difficulty of the route. eg: Very Hard
 * @param {string} anchor     A unique anchor point within the page for linking to. (Optional)
 * @param {string} youtube    The end of the URL to a youtube video.  https://youtu.be/7luVBKogBKk => 7luVBKogBKk (Optional)
 * @param {string} timestamp  The timestamp (in seconds) of a youtube video. (Optional)
 * @param {string} video      A local video file for exemplifying the route. Maximum of 8MB. (Optional)
 * @param {bool}   autoplay   Autoplays a video on loop if one is added. Video will be muted if this is applied. (Optional)
 * ? why is this recipe instead of combo. recipe can mean PC/PS combo recipe.
 * @param {string} combo      The series of inputs used to perform the combo. called recipe
 * @param {string} content    Text which explains the combo, theory, and application of the combo.
*
* @date 9/26/2023 - 9:43:26 PM
*
* @class TheoryBox
* @typedef {TheoryBox}
*/
/* eslint-enable max-len */

const { Util } = require('../utils');

class TheoryBox {
	static HEADER = 'TheoryBox';

	static VIDEO_SIZE = '256x192';

	/**
	 * parses a TheoryBox from wikitext, and returns a new TheoryBox Object.
	 *
	 * @date 9/27/2023 - 4:15:35 PM
	 *
	 * @static
	 * @param {*} theoryBoxString
	 */
	static fromString(theoryBoxString) {
		const trim = theoryBoxString.trim();
		Util.assert(trim.startsWith('{{'));
		Util.assert(trim.endsWith('}}'));

		const startRegex = /^\s*({{2})\s*(theorybox)\s*\|\s*/gi; // startswith: {{ theorybox |
		const endRegex   = /}}\s*$/gi;                           // endsWith: }}

		Util.assert(trim.match(startRegex));
		Util.assert(trim.match(endRegex));

		// remove `{{` and `}}` from the ends of the string.
		const noEnds = trim.slice(2, -2);

		const replaceChar = '�';
		// find all substrings that are surrounded by `{{` & `}}`
		const encodedString = noEnds.replaceAll(/{{.*?}}/gim, (match) => {
			// replace all `|` in them with replaceChar
			const fixedString = match.replaceAll('|', replaceChar);

			return fixedString;
		});

		// split by `|`
		const encodedArray = encodedString.split('|');

		// change replaceChar back to `|`
		const decodedArray = encodedArray.map((string) => {
			const decoded = string.replaceAll(replaceChar, '|').trim();
			return decoded;
		});

		// decodedArray now holds strings of format `key = value`
		console.log(decodedArray);

		const theoryBoxObject = {};
		decodedArray.forEach((string) => {
			// split at the first `=`
			const index = string.indexOf('=');
			const key   = string.slice(0, index);
			const value = string.slice(index + 1);

			// skip empty values & header keyword
			if (index !== -1 && key && value) {
				theoryBoxObject[key.trim().toLowerCase()] = value.trim();
			}
		});

		// rename recipe to combo for consistency
		theoryBoxObject.combo  = theoryBoxObject.recipe;
		theoryBoxObject.recipe = undefined;

		return new TheoryBox(theoryBoxObject);
	}

	constructor({
		title,
		oneliner,
		damage,
		difficulty,
		anchor,
		youtube,
		timestamp,
		video,
		autoplay,
		combo,
		content,
	}) {
		this.title      = title;
		this.oneliner   = oneliner;
		this.damage     = damage;
		this.difficulty = difficulty;
		this.anchor     = anchor;
		this.youtube    = youtube;
		this.timestamp  = timestamp;
		this.video      = video;
		this.autoplay   = autoplay;
		this.combo      = combo;
		this.content    = content;
	}

	toString() {
		const {
			title,
			oneliner,
			damage,
			difficulty,
			anchor,
			youtube,
			timestamp,
			video,
			autoplay,
			combo,
			content,
		} = this;

		/* eslint-disable max-len */
		const damageString   = damage             ? `\n<br>${damage} Damage` : '';
		const sizeString     = (video || youtube) ? `\n| Size       = ${TheoryBox.VIDEO_SIZE}` : '';
		const autoPlayString = autoplay           ? '\n| Autoplay   = yes' : '';
		const videoString    = video              ? `\n| Video      = ${video}` : '';
		const youtubeString  = youtube            ? `\n| Youtube    = ${youtube}\n| Timestamp  = ${timestamp}` : '';
		/* eslint-enable max-len */

		return `
{{TheoryBox
| Title      = ${title}
| Oneliner   = ${oneliner}${damageString}
| Difficulty = ${difficulty}
| Anchor     = ${anchor}${videoString}${youtubeString}${sizeString}${autoPlayString}
| Recipe     = ${combo}
| content    =
${content}
}}
`;
	}
}

module.exports = { TheoryBox };
