/**
 * @file        	\src\wikiObjects\Color.js
 * @fileoverview
 * 	class for dealing with the wikitext Color object.
 * 	the wikitext is formatted as: `{{clr|X|Y|game=Z}}`
 * 	`X` is the color identifier code.
 * 	`Y` is the text to be styled.
 * 	`game=Z` is optional, and forces the color of a specific game's button.
 * 	see the @link for a list of valid `Z` values.
 * 	If `Z` is not provided, it will assume the game from the RootPageName of the current page,
 * 			if not found, default to Guilty Gear's button colors.
 *
 * todo: a lot of the colorize/uncolorize functions have repeated code.
 *
 * @see <https://www.dustloop.com/w/Template:Color/doc>
 *
 * @author	pdpt
 * @date  	2023-09-30
 */

const { Util }    = require('../utils');
const { RegexCollection } = require('../RegexCollection');

class Color {
	/**
	 * @summary A list of potential code values.
	 * @description
	 * * Every value in a row renders as the same color.
	 * * Every value in a column is of the same format type.
	 * * `[0][0] = 1`, `[0][1] = `P`;
	 *   * Each of these will be colored as the hex code `#d96aca` (`[0][2]`);
	 * * All `[n][1]` values are button names.
	 *
	 * @date    	2023-10-01
	 * @static
	 * @memberof	Color
	 * @type    	{[[string|Number]]}
	 */
	static CODE_ARRAY = [
		[1,	'P',  '#d96aca', 'Very Easy'],
		[2,	'K',  '#1ba6ff', 'Easy'],
		[3,	'S',  '#16df53', 'Medium'],
		[4,	'H',  '#ff0000', 'Hard'],
		[5,	'D',  '#e8982c', 'Very Hard'],
	];

	/**
	 * `Color.KEY` stores the column associated with each format type.
	 * * `Color.KEY.button 1` because all `[n][1]` values are button names.
	 * @date 9/30/2023 - 8:20:38 PM
	 *
	 * @static
	 * @type {{ number: number; button: number; hex: number; difficulty: number; }}
	 */
	static KEY = Object.freeze({
		number     : 0,
		button     : 1,
		hex        : 2,
		difficulty : 3,
	});

	/* eslint-disable unicorn/better-regex */
	static SKIP_REGEX = [
		// skip comments
		/(\s*<!--.*?-->\s*)/dgm,
		// skip tabber title lines
		// lines that end with =
		/\n(.*=\s*)\n/dgm,
		// skip colored text
		/{{2}[cloru]{3,6}\|.*?\|.*?}{2}/d,
	];
	/* eslint-enable unicorn/better-regex */

	/* eslint-disable unicorn/better-regex */
	/**
	 * @description
	 * @date 2023-10-01
	 * @static
	 * @memberof Color
	 * @type {{regex:RegExp}}
	 */
	static REGEX = Object.freeze({
		// foo {{clr|<KEY>|<TEXT>}} bar => {{clr|<KEY>|<TEXT>}}
		coloredText         : /{{2}[cloru]{3,6}\|.*?\|.*?}{2}/d,
		// {{clr|<KEY>|<TEXT>}} => <TEXT>
		wordFromColoredWord : /(?<={{2}[cloru]{3,6}\|.*?\|)(.*?)(?=}{2})/d,
		// finds 'button' words: 236S, 5[D], ...
		// avoids text that is inside {{clr|a|b}}
		findButtonWord      : /(?<!{{2}[cloru]{3,6}\|.+\|)[!.1-9[cfj{]+[DHKPS]+[\]}]?(?![\]}]{1,3})/d,
		// finds the 'difficulty' words: Very Easy, Medium, ...
		findDiffWord        : /(Easy)|(Very Easy)|(Medium)|(Hard)|(Very Hard)(?!\|)(?!})/d,
		// j.236[D] => D
		buttonWordToKey     : /(?<=(?:!|[.1-9cfj]+){?\[?)([A-Z])(?=]?}?)/d,
		// finds letters [PKSHD] without the button prefix & surrounded by non-word characters
		// plus some specific stuff to avoid
		findLoneWord        : /(?<=\W)(?<![!.1-9[cfj{]+)(?<!\|)[DHKPS](?=\W)/d,
	});
	/* eslint-enable unicorn/better-regex */

	/**
	 * Apply color formatting to the word.
	 *
	 * `Y` => `{{clr|X|Y}}`
	 * @date 9/30/2023 - 12:07:27 AM
	 *
	 * @static
	 * @param {string} word
	 * @param {Color.KEY} [keyType] 			the code format to apply.
	 */
	static colorWord(word, keyType = Color.KEY.button) {
		const wordCode = Color.getCodeFromWord(word);
		const newCode  = Color.convertCodeToType(wordCode, keyType);
		const newWord  = `{{clr|${newCode}|${word}}}`;

		return newWord;
	}

	/**
	 * @description
	 *   given a code & key type,
	 *   returns a new code of the new type.
	 *
	 * @date 2023-10-01
	 * @static
	 * @param  {string | number} code the original code
	 * @param  {string | number} type the Color.KEY type of the new code
	 * @return {string | number} the new code
	 * @memberof Color
	 */
	static convertCodeToType(code, type) {
		// if code is a string that is a number, convert it to a number.
		// eslint-disable-next-line no-param-reassign
		code = Number(code) || code;
		const matchRow = Color.CODE_ARRAY.find((row) => {
			const rowMatch = row.find((cell) => {
				const  isMatch = (code === cell);
				return isMatch;
			});
			return rowMatch;
		});
		Util.assert(matchRow, `invalid convertCode: ${matchRow}`);
		// eslint-disable-next-line security/detect-object-injection
		const  match = matchRow[type];
		return match;
	}

	/**
	 * Remove color formatting from a word.
	 *
	 * `{{clr|X|Y|game=Z}}`=> `Y`
	 * @date 9/30/2023 - 12:22:25 AM
	 *
	 * @static
	 * @param {string} word
	 */
	static uncolorWord(word) {
		const newWord = word.match(RegexCollection.wordFromColoredWord, '$1')[0];
		return newWord;
	}

	/**
	 * @description
	 * Gets the appropriate color code for an input string.
	 * * given wikitext: `{{clr|X|Y}}`
	 * * input `Y`, returns `X`
	 *
	 * for example,
	 * * `5P`   should give `P`
	 * * `Easy` should give `2`
	 * * `c.S`  should give `S`
	 *
	 * @date 9/30/2023 - 12:57:09 AM
	 *
	 * @static
	 * @private
	 * @param {string} word
	 */
	static getCodeFromWord(word) {
		if (Number.isInteger(word)) {
			return word;
		}

		Util.warn((typeof word === 'string'), 'word is not type string & is not int');

		if (word.length === 1) {
			return word;
		}

		const testDifficulty = RegexCollection.difficultyWord.test(word);
		const testButton     = RegexCollection.buttonWord.test(word);

		switch (true) {
		case (word.startsWith('#')): {
			return word;
		}
		case (testDifficulty): {
			return word;
		}
		case (testButton): {
			return word.match(RegexCollection.buttonWordToKey)[0];
		}
		default: {
			console.log('word :>>', word);
			Util.assert(false, 'could not find valid code from word!');
			throw new Error('could not find valid code from word!');
		}
		}
	}

	static convertColorWord(word, type) {
		// {{clr|KEY|WORD}}
		const { keyFromColoredWord } = RegexCollection;
		const key                 = word.match(keyFromColoredWord)[0];
		const convertedKey        = Color.convertCodeToType(key, type);
		const convertedWord       = word.replace(key, convertedKey);

		return convertedWord;
	}

	/**
	 * Convert from one code to another, that have the same underlying color.
	 * Any row in `Color.CODE_ARRAY` points to the same underlying color,
	 * and the index of the row is specified by `Color.KEY`
	 *
	 * `'Easy', Color.KEY.button` => `'K'`
	 *
	 * @date 9/29/2023 - 11:57:53 PM
	 *
	 * @param {string}    code
	 * @param {Color.KEY} key
	 *
	 * @static
	 * @private
	 * @returns {string}
	 */
	// static convertCodeToType(code, key) {
	// 	const returnValue = Color.COLOR_ARRAY.find((color) => color.includes(code));

	// 	// eslint-disable-next-line security/detect-object-injection
	// 	return returnValue?.[key] || console.warn('color not found!');
	// }

	/* eslint-disable unicorn/prevent-abbreviations */
	/** @param {string} code  @returns {string} */
	static codeToNumber(code)     { return Color.convertCodeToType(code, Color.KEY.number); }
	/** @param {string} code  @returns {string} */
	static codeToButton(code)     { return Color.convertCodeToType(code, Color.KEY.button); }
	/** @param {string} code  @returns {string} */
	static codeToHex(code)        { return Color.convertCodeToType(code, Color.KEY.hex); }
	/** @param {string} code  @returns {string} */
	static codeToDifficulty(code) { return Color.convertCodeToType(code, Color.KEY.difficulty); }
	/* eslint-enable unicorn/prevent-abbreviations */

	static findUncoloredWord(string) {
		// get all regexes for finding matches
		const { buttonWord, difficultyWord,  buttonLetter } = RegexCollection;
		const array    = [buttonWord, difficultyWord,  buttonLetter];
		const callback = (last, next) => last || next.exec(string)?.indices[0];
		const match    = array.reduce((last, next) => callback(last, next), false);

		if (match) {
			return { start: match[0], end: match[1] };
		}
		return false;
	}

	static wordFromColoredWord(string) {
		const regex = RegexCollection.coloredText;
		const match = regex.exec(string)?.indices[0];

		if (match) {
			return { start: match[0], end: match[1] };
		}
		return false;
	}

	/**
	 * @description adds color to a string
	 * * assumes there is nothing to skip
	 *
	 * @date 2023-10-02
	 * @static
	 * @param  {string} string the string to add color to
	 * @return {string}        the colored string
	 * @memberof Color
	 */
	static colorizeString(string) {
		// find a word to color
		const uncoloredIndexes = Color.findUncoloredWord(string);

		// if there is nothing to color
		if (!uncoloredIndexes) {
			// return the string
			return string;
		}

		// otherwise...
		// slice the string at the ends of the word
		const preString   = string.slice(0, uncoloredIndexes.start);
		const wordToColor = string.slice(uncoloredIndexes.start, uncoloredIndexes.end);
		const postString  = string.slice(uncoloredIndexes.end);

		// color the word
		const coloredWord = Color.colorWord(wordToColor);

		// format the non-skip string
		const preSkipFormatted  = Color.colorizeString(preString);
		const postSkipFormatted = Color.colorizeString(postString);

		// rejoin the substrings
		const  joinedString = `${preSkipFormatted}${coloredWord}${postSkipFormatted}`;
		return joinedString;
	}

	/**
	 * @description removes color from a string
	 * * assumes there is nothing to skip
	 *
	 * @date 2023-10-02
	 * @static
	 * @param  {string} string the string to remove color from
	 * @return {string}        the uncolored string
	 * @memberof Color
	 */
	static uncolorizeString(string) {
		// find a word to color
		const coloredIndexes = Color.wordFromColoredWord(string);

		// if there is nothing to color
		if (!coloredIndexes) {
			// return the string
			return string;
		}

		// otherwise...
		// slice the string at the ends of the word
		const preString   = string.slice(0, coloredIndexes.start);
		const wordToUncolor = string.slice(coloredIndexes.start, coloredIndexes.end);
		const postString  = string.slice(coloredIndexes.end);

		// color the word
		const coloredWord = Color.uncolorWord(wordToUncolor);

		// format the non-skip string
		const preSkipFormatted  = Color.uncolorizeString(preString);
		const postSkipFormatted = Color.uncolorizeString(postString);

		// rejoin the substrings
		const  joinedString = `${preSkipFormatted}${coloredWord}${postSkipFormatted}`;
		return joinedString;
	}

	/**
	 * @description converts all color objects in a string
	 * * assumes there is nothing to skip
	 *
	 * @date 2023-10-02
	 * @static
	 * @param  {string} string the string to find and convert color objects
	 * @return {string}        the uncolored string
	 * @memberof Color
	 */
	static convertColors(string, type) {
		// find a word to color
		const coloredIndexes = Color.wordFromColoredWord(string);

		// if there is nothing to color
		if (!coloredIndexes) {
			// return the string
			return string;
		}

		// otherwise...
		// slice the string at the ends of the word
		const preString     = string.slice(0, coloredIndexes.start);
		const wordToConvert = string.slice(coloredIndexes.start, coloredIndexes.end);
		const postString    = string.slice(coloredIndexes.end);

		// color the word
		const coloredWord = Color.convertColorWord(wordToConvert, type);

		// format the non-skip string
		const preSkipFormatted  = Color.convertColors(preString, type);
		const postSkipFormatted = Color.convertColors(postString, type);

		// rejoin the substrings
		const  joinedString = `${preSkipFormatted}${coloredWord}${postSkipFormatted}`;
		return joinedString;
	}

	/**
	 * returns colorized string,
	 * * there may be sections in the string to skip over.
	 * * string may be multi-line, or an entire document.
	 *
	 * @date 9/30/2023 - 7:48:41 PM
	 *
	 * @static
	 * @private
	 * @param {string} string
	 * @returns {string}
	 */
	static colorizeWithSkips(string) {
		const regex       = RegexCollection.doNotColor();
		const splitString = Util.getSplitString(string, regex);

		if (!splitString) {
			return Color.colorizeString(string);
		}

		const [pre, skip, post] = splitString;

		// format the non-skip string
		const preFormatted  = Color.colorizeWithSkips(pre);
		const postFormatted = Color.colorizeWithSkips(post);

		// rejoin the substrings
		const joinedString = `${preFormatted}${skip}${postFormatted}`;
		return joinedString;
	}

	/**
	 * @description  returns uncolorized string.
	 * * there may be sections in the string to skip over.
	 * * string may be multi-line, or an entire document.
	 *
	 * @date 2023-10-02
	 *
	 * @static
	 * @param   {string} string the string to uncolor
	 * @returns {string}        the uncolored string
	 * @memberof Color
	 */
	static uncolorizeWithSkips(string) {
		// find a substring to skip.
		// these are the same as when colorizing, but not skipping colored text.
		// todo: this should be cleaner, probably have findColoredRegex defined not in SKIP_REGEX.
		const regex   = RegexCollection.doNotUncolor();
		const splitString = Util.getSplitString(string, regex);

		if (!splitString) {
			return Color.uncolorizeString(string);
		}

		const [pre, match, post] = splitString;
		// format the non-skip string
		const preFormatted  = Color.uncolorizeWithSkips(pre);
		const postFormatted = Color.uncolorizeWithSkips(post);

		// rejoin the substrings
		const joinedString = `${preFormatted}${match}${postFormatted}`;
		return joinedString;
	}

	/**
	 * @description
	 *   returns string with color objects of the specified format.
	 * * there may be sections in the string to skip over.
	 * * string may be multi-line, or an entire document.
	 *
	 * @date 2023-10-02
	 *
	 * @static
	 * @param   {string}     string the string to convert
	 * @param   {Colors.KEY} type   the type to convert to
	 * @returns {string}            the converted string
	 * @memberof Color
	 */
	static convertColorsWithSkips(string, type) {
		// find a substring to skip.
		// these are the same as when colorizing, but not skipping colored text.
		const { doNotFormat } = RegexCollection;
		const splitString     = Util.getSplitString(string, doNotFormat());

		if (!splitString) {
			return Color.convertColors(string, type);
		}

		const [pre, match, post] = splitString;

		// format the non-skip string
		const preFormatted  = Color.convertColorsWithSkips(pre, type);
		const postFormatted = Color.convertColorsWithSkips(post, type);

		// rejoin the substrings
		const joinedString = `${preFormatted}${match}${postFormatted}`;
		return joinedString;
	}

	/**
	 * takes an arbitrary wikitext string, and applies the color formatting to it.
	 * * input:  `2K > 236S > 5H`
	 * * output: `{{clr|K|2K}} > {{clr|S|236S}} > {{clr|H|5H}}`
	 *
	 * @date 2023-10-01
	 * @static
	 * @param  {string} string		the uncolored wikiText to be colorized
	 * @return {string}						the colored wikiText, that has color.
	 * @memberof Color
	 */
	static colorizeWikiText(string) {
		const  coloredText = Color.colorizeWithSkips(string);
		return coloredText;
	}

	/**
	 * @description
	 *   takes an arbitrary wikitext string, and removes color formatting from it.
	 *   ignores the same things as the colorizer, so you can have example formatting in comments.
	 *
	 * @date 2023-10-02
	 * @static
	 * @param   {string} string  the colored wikiText to be uncolored
	 * @returns {string}         the uncolored wikiText
	 * @memberof Color
	 */
	static uncolorizeWikiText(string) {
		const uncoloredText = Color.uncolorizeWithSkips(string);
		return uncoloredText;
	}

	/**
	 * @description
	 *   takes an arbitrary wikitext string,
	 *   and converts all color objects to hex format.
	 *
	 * @date 2023-10-02
	 * @static
	 * @param   {string} string  the wikiText to have it's colors converted
	 * @returns {string}         the converted wikiText
	 * @memberof Color
	 */
	static wikiColorsToHex(string) {
		const hexText = Color.convertColorsWithSkips(string, Color.KEY.hex);
		return hexText;
	}

	/**
	 * @description
	 *   takes an arbitrary wikitext string,
	 *   and converts all color objects to button format.
	 *
	 * @date 2023-10-02
	 * @static
	 * @param   {string} string  the wikiText to have it's colors converted
	 * @returns {string}         the converted wikiText
	 * @memberof Color
	 */
	static wikiColorsToButton(string) {
		const  buttonText = Color.convertColorsWithSkips(string, Color.KEY.button);
		return buttonText;
	}
}

module.exports = { Color };
