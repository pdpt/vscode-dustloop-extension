// Tabber.js;

/**
 * `<tabber>`
 * `TabName =`
 * `{| class="wikitable sortable|}`
 * `|-|`
 * `Tabname=...`
 * `</tabber>`
 *
 * @date 9/26/2023 - 10:07:40 PM
 *
 * @class Tabber
 * @typedef {Tabber}
 *
 * @param {Array[{}]} [tabs]	collection of objects each with a tabName & tabContent.
 */
class Tabber {
	constructor(tabs = []) {
		this.tabs = tabs;
	}

	addTab({ tabName, tabContent }) {
		this.tabs.push({ tabName, tabContent });
	}

	toString() {
		// first tab is different because it doesn't start with `|-|`
		const firstTab   = this.tabs.shift();

		const tabStrings = this.tabs.map((tab) => {
			const { tabName, tabContent } = tab;
			return `|-|\n${tabName} = ${tabContent}`;
		}).join('\n');

		return `<tabber>
${firstTab.tabName} = ${firstTab.tabContent}
${tabStrings}
</tabber>
`;
	}
}

module.exports = { Tabber };
