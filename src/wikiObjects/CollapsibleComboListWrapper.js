// CollapsibleComboListWrapper.js

/**
 * Collapsible Combo List Wrapper
 * @link <https://www.dustloop.com/w/Template:Collapsible_Combo_List_Wrapper/doc>
 *
 * @date 9/26/2023 - 9:40:18 PM
 *
 * @class CollapsibleComboListWrapper
 * @typedef {CollapsibleComboListWrapper}
 */
class CollapsibleComboListWrapper {
	constructor(headerString, tabber) {
		this.headerString = headerString;
		this.tabber = tabber;
	}
	toString() {
		const { headerString, tabber } = this;
		return `
{{Collapsible Combo List Wrapper
|Header    = ${headerString}
|ComboList = ${tabber}
}}
`;
	}
}

module.exports = { CollapsibleComboListWrapper };
