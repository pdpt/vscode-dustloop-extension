/**
 * collection of GGSTComboTableRows
 * @date 9/26/2023 - 10:31:13 PM
 * {| class="wikitable sortable"
 * {{GGST-ComboTableHeader}}
 * |-
 * {{GGST-ComboTableRow...}}
 * |-
 * {{GGST-ComboTableRow
 * ...
 * }}
 * |-
 * |}
 *
 * @class GGSTComboTable
 * @typedef {GGSTComboTable}
 */
class GGSTComboTableSortable {
	/**
	 * Creates an instance of GGSTComboTable.
	 * @date 9/26/2023 - 10:31:23 PM
	 *
	 * @constructor
	 * @param {[GGSTComboTableRows]} [rows] array of rows
	 */
	constructor(rows = []) {
		this.rows = rows;
	}

	add(newRow) {
		this.rows.push(newRow);
	}
}

module.exports = { GGSTComboTableSortable };
