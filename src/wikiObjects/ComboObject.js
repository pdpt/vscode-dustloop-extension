/* eslint-disable unicorn/better-regex */
// ComboObject.js;
const { GGSTComboTableRow } = require('./GGSTComboTableRow');
const { RegexCollection }   = require('../RegexCollection');
const { TheoryBox }         = require('./TheoryBox');
const { Util }              = require('../utils');

/**
 * object to hold all information about a combo, for displaying as either a TheoryBox, table row, or export as a CSV.
 * @date 9/27/2023 - 12:35:59 AM
 *
 * combo info params:
 * @param  {string} combo          		The series of inputs used to perform the combo.
 * @param  {string} position       		Any positioning restriction. Corner, Midscreen, Anywhere
 * @param  {string} difficulty     		A brief descriptor of the approximate difficulty of the route.
 * 																		eg: [Very Easy, Easy, Medium, Hard, Very Hard]
 * @param  {string} damage         		Total damage the combo does.
 * 																		Conventionally tested against Ky Kiske
 * @param  {string} worksOn        		Any character restrictions.
 * 																		Some combos don't work on lightweights
 * 																		Some whiff on short characters like Giovanna
 * @param  {string} tensionGain    		The amount of tension gained by performing the combo.
 * @param  {string} setup          		Describes how you create the setup to land the combo.          [OPTIONAL]
 * @param  {string} recipe         		Recipe id for the in-game combo maker of the game
 * @param  {string} checkedVersion 		Last version of the game this combo was tried and confirmed to be working.
 * 																		At the time of writing, the last released game version is 1.23,
 * 																		the last balance patch is 1.18
 * wiki meta params:
 * @param  {string} anchor         		A unique anchor point within the page for linking to.          [Optional]
 *
 * video params:
 * @param  {string} video          		A local video file for exemplifying the route. Maximum of 8MB. [Optional]
 * @param  {string} youtube        		The end of the URL to a youtube video.                         [Optional]
 * 																		It looks like this 7luVBKogBKk [from https://youtu.be/7luVBKogBKk].
 * @param  {string} timestamp      		The timestamp (in seconds) of a youtube video.                 [Optional]
 * @param    {bool} autoplay       		Autoplays a video on loop if one is added.                     [Optional]
 * 																		Video will be muted if this is applied.
 * text description params:
 * @param  {string} title          		A name or label for the route. Appears in bold text.
 * @param  {string} oneliner       		A short, one-sentence descriptor for the route. Appears in italics.
 * @param  {string} notes          		Free form explanation of important points pertaining to the combo.
 * 																		Timing aspects, input tricks, gameplay significance, drawbacks, whatever.
 * @param  {string} content        		Text which explains the combo, theory, and application of the combo.
 *
 * @class ComboObject
 * @typedef {ComboObject}
 */

class ComboObject {
	static HEADERS = ['GGST-ComboTableRow', 'TheoryBox'];
	constructor({
		combo,
		position,
		difficulty,
		damage,
		workson,
		tensiongain,
		setup,
		recipe,
		checkedversion,
		anchor,
		video,
		youtube,
		timestamp,
		autoplay,
		title,
		oneliner,
		notes,
		content,
	} = {}) {
		this.combo          = combo          || '';
		this.position       = position       || '';
		this.difficulty     = difficulty     || '';
		this.damage         = damage         || '';
		this.workson        = workson        || '';
		this.recipe         = recipe         || '';
		this.checkedversion = checkedversion || '';
		this.tensiongain    = tensiongain    || '';
		this.anchor         = anchor         || '';
		this.video          = video          || '';
		this.youtube        = youtube        || '';
		this.timestamp      = timestamp      || '';
		this.autoplay       = autoplay       || false;
		this.setup          = setup          || '';
		this.title          = title          || '';
		this.oneliner       = oneliner       || '';
		this.notes          = notes          || '';
		this.content        = content        || '';
	}

	/**
	 * @description
	 *   returns a ComboObject from a string of wikitext
	 * * assumes there is no text to skip
	 * * assumes there is only one object
	 * * checks that string starts with `{{TheoryBox` or `{{GGST-ComboTableRow` and ends with `}}`
	 *
	 * @date 2023-10-04
	 *
	 * @static
	 * @param   {string}      string the wikitext string to parse
	 * @returns {ComboObject}        the new combo object from the parsed wikitext
	 * @memberof ComboObject
	 */
	static getSingleObjectFromString(string) {
		Util.assert(typeof string === 'string', `${string} is not a string!`);
		// find header
		Util.warn(/^{{((TheoryBox)|(GGST-ComboTableRow))/.test(string.trim()), `${string} did not pass startsWith`);
		Util.warn(string.trim().endsWith('}}'), 'endsWith');

		if (/^{{(TheoryBox)/.test(string.trim())) {
			const newTheoryBox = TheoryBox.getSingleObjectFromString(string);
			return ComboObject.fromTheoryBox(newTheoryBox);
		}
		if (/^{{(GGST-ComboTableRow)/.test(string.trim())) {
			const newTableRow = GGSTComboTableRow.getSingleObjectFromString(string);
			return ComboObject.fromTableRow(newTableRow);
		}
		Util.warn(false, `${string} is unknown object type!`);

		const trimmed = string.trim().slice(0, -2);
		const constructorObject = {};

		// split by `|`
		// skipping based on regex
		const splitArray = Util.splitWithSkips(trimmed, '|', /{{.*?}}/dg);

		// split each part into key,val pairs
		splitArray.forEach((substring) => {
			// split at the first '='
			const split = Util.getSplitString(substring.trim(), '=');
			// only try to assign if it's a valid string
			// skip the header basically
			if (split) {
				const [key,, value] = split;
				// assign key,val pairs
				let trim = key.trim().toLowerCase();
				if ((trim === 'recipepc' || trim === 'recipeps') && !constructorObject.recipe) {
					trim = 'recipe';
				}
				// eslint-disable-next-line security/detect-object-injection
				constructorObject[trim] = value.trim();
			}
		});
		// return object.
		return new ComboObject(constructorObject);
	}

	/**
	 * @description
	 *   returns a an array of ComboObjects from a string of wikitext
	 * * assumes there is no text to skip
	 *
	 * @todo need to parse theorybox & combotable differently because they use 'recipe' differently.
	 *
	 * @date 2023-10-04
	 * @static
	 * @param {*} string
	 * @return {*}
	 * @memberof ComboObject
	 */
	static getObjectsFromString(string) {
		// todo
		// split by objects
		const comboArray = [];
		let   matches    = [];
		// get a list of all instances of `{{<prefix>`
		ComboObject.HEADERS.forEach((header) => {
			// eslint-disable-next-line security/detect-non-literal-regexp
			const regex   = new RegExp(`{{${header}`, 'gdi');
			// const result  = regex.exec(string);
			// const map     = result?.indices?.map((value) => value[0]);
			const allMatches = string.matchAll(regex);
			const array = [...allMatches].map((item) => item.indices[0][0]);

			matches = [...matches, ...(array || [])];
		});

		console.log('matches :>>', matches);

		matches.forEach((start) => {
			console.group('findSurroundedBy');
			// start a recursive function to find the end of the object.
			const { end }     = Util.findSurroundedBy(string, start, string.length, '{{', '}}');
			const slice       = string.slice(start, end).trim();
			const comboObject = ComboObject.getSingleObjectFromString(slice);

			comboArray.push(comboObject);
			console.groupEnd();
		});

		// return the collection of objects.
		Util.isArray(comboArray);
		return comboArray;
	}

	static getObjectsWithSkips(string) {
		const regex       = RegexCollection.skipComments;
		const splitString = Util.getSplitString(string, regex);

		if (!splitString) {
			return ComboObject.getObjectsFromString(string);
		}

		const [pre,, post] = splitString;

		// rejoin without the skip
		const joinedString = `${pre}${post}`;
		// parse the new string
		const comboObjectArray = ComboObject.getObjectsWithSkips(joinedString);
		Util.isArray(comboObjectArray);
		return comboObjectArray;
	}

	/**
	 * @description
	 *   parses a string for wikitext combos,
	 *   returns a collection of ComboObjects
	 *
	 * @date 2023-10-02
	 * @static
	 * @param  {string} wikiText the wikitext to parse
	 * @return {[ComboObject]}   the collection of combo objects parsed from the wikitext.
	 * @memberof ComboObject
	 */
	static fromWikiText(wikiText) {
		const  comboObjects = ComboObject.getObjectsWithSkips(wikiText);
		Util.isArray(comboObjects);
		return comboObjects;
	}

	toTheoryBox() {
		const theoryBoxObject = {
			title      : this.title,
			oneliner   : this.oneliner,
			damage     : this.damage,
			difficulty : this.difficulty,
			anchor     : this.anchor,
			youtube    : this.youtube,
			timestamp  : this.timestamp,
			video      : this.video,
			autoplay   : this.autoplay,
			combo      : this.combo,
			content    : this.content,
		};

		return new TheoryBox(theoryBoxObject);
	}

	toTableRow() {
		const tableRowObject = {
			combo          : this.combo,
			position       : this.position,
			damage         : this.damage,
			workson        : this.workson,
			difficulty     : this.difficulty,
			tensiongain    : this.tensiongain,
			notes          : this.notes,
			video          : this.video,
			setup          : this.setup,
			recipe         : this.recipe,
			checkedversion : this.checkedversion,
		};
		return new GGSTComboTableRow(tableRowObject);
	}

	static toTable(array) {
		// eslint-disable-next-line max-len
		const styling = 'class="wikitable sortable" border="1" style="min-width: 1000px; style="margin: 1em auto 1em auto;text-align: center"';
		const header = `{| ${styling}\n|+Table Heading Here\n{{GGST-ComboTableHeader}}\n|-\n`;
		const footer = '\n|}';
		const mapped = array.map((item) => {
			const row = item.toTableRow();
			const string = row.toString().trim();
			return string;
			// (item.toTableRow()).toString()
		});
		const body = mapped.join('\n|-\n');
		return `${header}${body}${footer}`;
	}
}

module.exports = { ComboObject };
