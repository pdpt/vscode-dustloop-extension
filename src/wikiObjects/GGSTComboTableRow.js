/**
 * table should have tabs for:
 *  * all combos
 *  * {anywhere, midscreen, corner, backCorner}
 * 	* basic, core, situational combos
 */

const { Util } = require('../utils');

// GGSTComboTableRow.js;
/**
 * {{GGST-ComboTableRow
 * |combo={{clr|2|5K}} > {{clr|1|6P}} > {{clr|2|214K}}, {{clr|2|5K}} > {{clr|3|623S}} WS {{clr|5|5[D]}} WB
 * |position=Corner
 * |damage=166
 * |worksOn=Everyone
 * |difficulty=Medium
 * |tensionGain=28 //* is put in the notes section at the top.
 * |notes=Requires the enemy to be close to the corner for {{clr|3|623S}} not to whiff after {{clr|4|6H}}
 * |recipePC=
 * |recipePS= //* is hidden if `recipePC` is filled. all recipes work on all platforms
 * |checkedVersion=1.29}}
 */

/**
 * Description placeholder
 *
 * @link <https://www.dustloop.com/w/Template:GGST-ComboTableRow/doc>
 * @date 9/26/2023 - 10:03:29 PM
 *
 * @param {string} combo
 * @param {string} position
 * @param {number} damage
 * @param {string} worksOn
 * @param {string} difficulty
 * @param {number} tensionGain				 //* is put in the notes section at the top.
 * @param {string} notes
 * @param {string} recipe
 * @param {number} checkedVersion
 */
class GGSTComboTableRow {
	static HEADER = 'GGST-ComboTableRow';
	/**
	 * parses a GGSTComboTableRow from wikitext, and returns a new GGSTComboTableRow Object.
	 *
	 * @date 9/27/2023 - 4:15:35 PM
	 *
	 * @static
	 * @param {*} GGSTComboTableRowString
	 */
	static fromString(comboRowString) {
		const trim = comboRowString.trim();
		Util.assert(trim.startsWith('{{'));
		Util.assert(trim.endsWith('}}'));

		// eslint-disable-next-line max-len
		const startRegex = /^\s*({{2})\s*(ggst-combotablerow)\s*\|\s*/gi; // startswith: {{ ggst-combotablerow |
		const endRegex   = /}}\s*$/gi;                           // endsWith: }}

		Util.assert(trim.match(startRegex));
		Util.assert(trim.match(endRegex));

		const noEnds = trim.slice(2, -2);
		const replaceChar = '�';
		// find all substrings that are surrounded by `{{` & `}}`
		const encodedString = noEnds.replaceAll(/{{.*?}}/gim, (match) => {
			// replace all `|` in them with replaceChar
			const fixedString = match.replaceAll('|', replaceChar);

			return fixedString;
		});

		// split by `|`
		const encodedArray = encodedString.split('|');

		// change replaceChar back to `|`
		const decodedArray = encodedArray.map((string) => {
			const decoded = string.replaceAll(replaceChar, '|').trim();
			return decoded;
		});

		// decodedArray now holds strings of format `key = value`
		console.log(decodedArray);

		const constructorObject = {};
		decodedArray.forEach((string) => {
			// split at the first `=`
			const index = string.indexOf('=');
			let   key   = string.slice(0, index).trim().toLowerCase();
			const value = string.slice(index + 1).trim();

			// convert pc/ps recipe to generic recipe
			// prefer whichever was listed first.
			// eslint-disable-next-line security/detect-object-injection
			if ((key === 'recipepc' || key === 'recipeps')
			&& !constructorObject.recipe) {
				key = 'recipe';
			}

			// skip empty values & header keyword
			if (index !== -1 && key && value) {
				// eslint-disable-next-line security/detect-object-injection
				constructorObject[key] = value;
			}
		});

		return new GGSTComboTableRow(constructorObject);
	}

	constructor({
		combo          = '',
		position       = '',
		damage         = Number.NaN,
		workson        = 'Everyone',
		difficulty     = '',
		tensiongain    = Number.NaN,
		notes          = '',
		recipe         = '',
		checkedversion = Number.NaN,
	}) {
		// super(combo);
		this.combo          = combo;
		this.position       = position;
		this.damage         = damage;
		this.workson        = workson;
		this.difficulty     = difficulty;
		this.tensiongain    = tensiongain;
		this.notes          = notes;
		this.recipe         = recipe;
		this.checkedversion = checkedversion;
	}

	toString() {
		return this.getWikiText();
	}

	getWikiText() {
		const {
			combo,
			position,
			damage,
			workson,
			difficulty,
			tensiongain,
			notes,
			recipe,
			checkedversion,
		} = this;
		return `
{{GGST-ComboTableRow
	|combo          = ${combo}
	|position       = ${position}
	|damage         = ${damage}
	|worksOn        = ${workson}
	|difficulty     = ${difficulty}
	|tensionGain    = ${tensiongain}
	|notes          = ${notes}
	|recipePC       = ${recipe}
	|checkedVersion = ${checkedversion}
}}`;
	}

	getWikiTextCollapsed() {
		const text = this.getWikiText();
		text.replaceAll(/(\n\s*\|)/g, ' |');
	}
}

module.exports = { GGSTComboTableRow };
