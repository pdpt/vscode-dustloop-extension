# wikiObjects

Objects to parse, or format, that will have their own class:

* TheoryBox
  * <https://www.dustloop.com/w/Template:TheoryBox/doc>
* Collapsible Combo List Wrapper -
  * `{{Collapsible Combo List Wrapper`
  * `|Header = HeaderString`
  * `|ComboList = <tabber>`
  * `</tabber>}}`
  * <https://www.dustloop.com/w/Template:Collapsible_Combo_List_Wrapper/doc>
* tabber -
  * `<tabber>`
  * `TabName =`
  * `{| class="wikitable sortable|}`
  * `|-|`
  * `Tabname=...`
  * `</tabber>`
  * <https://www.dustloop.com/w/Template:Collapsible_Combo_List_Wrapper/doc>
* GGST-ComboTable (wikitable sortable) -
  * `{| class="wikitable sortable"`
  * `{{GGST-ComboTableHeader}}`
  * `|-`
  * `{{GGST-ComboTableRow ... }}`
  * `|-`
  * `...`
  * `|}`
* GGST-ComboTableRow -
  * <https://www.dustloop.com/w/Template:GGST-ComboTableRow/doc>
  * `{{GGST-ComboTableRow`
  * `|combo =`
  * `|position =`
  * `|damage =`
  * `|worksOn =`
  * `|difficulty =`
  * `|notes =`
  * `|recipePC =`
  * `|recipePS =`
  * `|video =`
  * `|checkedVersion =`
  * `}}`

Simple objects that will not have their own class:

* GGST-ComboTableHeader - {{GGST-ComboTableHeader}}
* BasicComboDef         - {{BasicComboDef}}
* CoreComboDef          - {{CoreComboDef}}
* SpecializedComboDef   - {{SpecializedComboDef}}
* Anchor                - {{Anchor|`NAME`}}
* Color or Clr - {{color|`X`|`Y`|game=Z}} or {{clr|`X`|`Y`|game=Z}}
  * <https://www.dustloop.com/w/Template:Color/doc>
  * The first parameter (X) is the color identifier code.
  * The second parameter (Y) is the text to be styled.
  * Optional, (Z) can be used to a specific game's colors.
* Template:ComboText - {{ComboText|`Text`}}
  * Creates a `<span>` which immitates the appearance of a `<code>` block with modified styling.
  * <https://www.dustloop.com/w/Template:ComboText/doc>

## combo data

combos are mostly displayed in 2 ways, in a TheoryBox or as part of a table.

they share these values:

* combo
  * called recipe in TheoryBox
* [notes, content, oneliner]
  * these are all freeform text descriptions,
  * not the same, but similar
* damage
* difficulty

with these being unique to TheoryBox

* title
* anchor
* youtube
* timestamp  
* video
* autoplay

and these unique to table rows

* position
* worksOn
* tensiongain
* recipe
  * different from TheoryBox recipe, which is called combo in the table rows.
* checkedVersion

There will be a unified class called `ComboObject` to store all this information. Any extra information I find useful may also be added to the ComboObject.

The ComboObject will have methods for returning the data as either format, TheoryBox or table row. Possibly also CSV to export combos to a spreadsheet.

## readable wiki format

### tables

tables in the wiki will generally be of the format:

```php
{{Collapsible Combo List Wrapper
  |Header = 
  |ComboList =  
  <tabber>
    TabName =
    {| class="wikitable sortable"
      {{GGST-ComboTableHeader}}
      |-
      {{GGST-ComboTableRow 
        |combo =
        |position =
        |damage =
        |worksOn =
        |difficulty =
        |notes =
        |recipePC =
        |video =
        |checkedVersion =
      }}
      |-
      {{GGST-ComboTableRow }}
    |}
    |-|
    Tabname=...
  </tabber>
}}
```

if we try to minimize this text, while still keeping values on their own line:

```php
{{Collapsible Combo List Wrapper
  |Header = 
|ComboList = <tabber>
  TabName = 
{|class="wikitable sortable"{{GGST-ComboTableHeader}}|-{{GGST-ComboTableRow 
  |combo =
  |position =
  |damage =
  |worksOn =
  |difficulty =
  |notes =
  |recipePC =
  |video =
  |checkedVersion =
}}|-{{GGST-ComboTableRow }}|}|-|
  Tabname = 
{| class= ...
  ...
|}</tabber>}}
```

we go from 25 lines to 18, saving 7 lines. This probably isn't worth it.

if we look at an example table row:

```php
{{GGST-ComboTableRow
|combo = (66) {{clr|1|5P}}/{{clr|1|2P}}/{{clr|2|5K}}/{{clr|2|2K}} > {{clr|1|6P}} (> {{clr|2|214K}})
|position = Anywhere 
|damage = 70 (73) [76]
|worksOn = Everyone
|difficulty = Very Easy
|notes = Close range {{clr|1|P}}/{{clr|2|K}} confirm. {{clr|1|6P}} > {{clr|2|214K}} is guaranteed in the corner, but very close range or dash momentum is needed midscreen for the combo to connect midscreen. Damage given for {{clr|1|5P}} starter.
|recipePC = 52491
|checkedVersion = 1.19
}}
```

combo & notes are very long, while everything else is pretty short. If we give them dedicated lines, abbreviate, and merge everything else into a few lines:

```php
{{GGST-ComboTableRow
|combo = (66) {{clr|1|5P}}/{{clr|1|2P}}/{{clr|2|5K}}/{{clr|2|2K}} > {{clr|1|6P}} (> {{clr|2|214K}})
|pos   = Anywhere |dmg = 70 (73) [76]|on = Everyone|diff = Very Easy |code = 52491|ver = 1.19
|notes = Close range {{clr|1|P}}/{{clr|2|K}} confirm. {{clr|1|6P}} > {{clr|2|214K}} is guaranteed in the corner, but very close range or dash momentum is needed midscreen for the combo to connect midscreen. Damage given for {{clr|1|5P}} starter.
}}
```

we can fit everything into less than 100 characters

```php
{{GGST-ComboTableRow
|combo   = (66) {{clr|1|5P}}/{{clr|1|2P}}/{{clr|2|5K}}/{{clr|2|2K}} > {{clr|1|6P}} (> {{clr|2|214K}})
|damage  = 70 (73) [76] |difficulty = Very Easy |recipePC       = 52491    
|worksOn = Everyone     |position   = Anywhere  |checkedVersion = 1.19
|notes   = Close range {{clr|1|P}}/{{clr|2|K}} confirm. {{clr|1|6P}} > {{clr|2|214K}} is guaranteed in the corner, but very close range or dash momentum is needed midscreen for the combo to connect midscreen. Damage given for {{clr|1|5P}} starter.
}}

{{GGST-ComboTableRow
|combo = (66) {{clr|1|5P}}/{{clr|1|2P}}/{{clr|2|5K}}/{{clr|2|2K}} > {{clr|1|6P}} (> {{clr|2|214K}})
|position   = Anywhere  |damage          = 70 (73) [76]
|worksOn    = Everyone  |recipePC        = 52491     
|difficulty = Very Easy |checkedVersion  = 1.19
|notes = Close range {{clr|1|P}}/{{clr|2|K}} confirm. {{clr|1|6P}} > {{clr|2|214K}} is guaranteed in the corner, but very close range or dash momentum is needed midscreen for the combo to connect midscreen. Damage given for {{clr|1|5P}} starter.
}}
```

giving them 2 or 3 lines makes them quite readable imo.

### TheoryBoxes

TheoryBoxes have the format:

```php
{{TheoryBox
{{clr|H|j.H}} > land {{clr|K|2K}}. It's not as threatening as her usual okizeme, but it's useful for stealing the corner if you happen to hit a combo while cornered.
| Title      = A name or label for the route. Appears in bold text.
| Oneliner   = A short, one-sentence descriptor for the route. Appears in italics.
| Difficulty = A brief descriptor of the approximate difficulty of the route. eg: Very Hard
| Anchor     = A unique anchor point within the page for linking to. (Optional)
| Youtube    = The end of the URL to a youtube video. It looks like this 7luVBKogBKk [from https://youtu.be/7luVBKogBKk]. (Optional)
| Timestamp  = The timestamp (in seconds) of a youtube video. (Optional)
| Video      = A local video file for exemplifying the route. Maximum of 8MB. (Optional)
| Autoplay   = Autoplays a video on loop if one is added. Video will be muted if this is applied. (Optional)
| Recipe     = The series of inputs used to perform the combo.
| content    = Text which explains the combo, theory, and application of the combo.
}}

```

here is an example:

```php
==Complex Example==
{{TheoryBox
| Title      = Optimal Banishing Strike Combo
| Oneliner   = {{CLabel|HNK|Toki}} variant
| Difficulty = {{clr|5|Extremely Hard}}
| Anchor     = OptimalTokiBani
| Video      = HNK Raoh Placeholder Video.mp4
| Size       = 256x192
| Recipe     = {{clr|5|2A}}x2 > {{clr|4|2B}} > {{clr|3|5D}} > C+D > 6 > {{clr|1|2C}} > slight delay 9E > {{clr|5|j.A}} > {{clr|4|j.B}} > {{clr|3|j.D}} > land then jump straight up > {{clr|5|j.A}} (right side only) > {{clr|3|j.214D}} > {{clr|5|2A}}x12 > {{clr|3|c.D}}(1) > A+C > {{clr|1|j.C}} > {{clr|3|j.214D}} > {{clr|5|2A}}x5 > {{clr|3|c.D}}(1) > A+C > {{clr|5|j.A}} > {{clr|3|j.214D}} > {{clr|5|2A}}x15 > {{clr|4|2B}} > {{clr|3|5D}} > {{clr|3|214D}}+E > {{clr|5|2A}}x14 > {{clr|5|2A}}~E > {{clr|5|2A}}x4 > {{clr|4|2B}} > {{clr|3|5D}} > {{clr|3|214D}}+E > {{clr|5|2A}}x10 > {{clr|4|2B}} > {{clr|1|c.C}} > C+D > 6 > {{clr|4|2B}} > {{clr|5|2A}}x5 > {{clr|5|2A}}~E > {{clr|5|2A}}x14 > {{clr|5|A}}xN~ (Traveling)
| content    =
An optimal, 5-hit-bani combo which has been been fine-tuned to balance ease of use with Boost recovery while guaranteeing a ToD.

Like all optimal bani routes, this combo assumes that you have no starting resources and start the combo with a grounded hit into 5-hit-bani. You can still route into something similar to this by using the standard combo pieces of {{Keyword|Ura Sai}} > {{Keyword|Renda Sai}}. The advantage of this route over that kind of combo is that the less-optimal routes involve more TK Sais and take much longer to start cranking out resources. TK Sai is the most common point to drop a Raoh combo, so minimizing how many are needed is a huge boon to your combo.
}}
```

again we can see the recipe and content are quite large, and need their own lines. lets try to combine a few other fields like we did with the table

```php
==Complex Example==
{{TheoryBox
| Title      = Optimal Banishing Strike Combo | Oneliner = {{CLabel|HNK|Toki}} variant    | Anchor = OptimalTokiBani
| Difficulty = {{clr|5|Extremely Hard}}       | Video    = HNK Raoh Placeholder Video.mp4 | Size   = 256x192
| Recipe     = {{clr|5|2A}}x2 > {{clr|4|2B}} > {{clr|3|5D}} > C+D > 6 > {{clr|1|2C}} > slight delay 9E > {{clr|5|j.A}} > {{clr|4|j.B}} > {{clr|3|j.D}} > land then jump straight up > {{clr|5|j.A}} (right side only) > {{clr|3|j.214D}} > {{clr|5|2A}}x12 > {{clr|3|c.D}}(1) > A+C > {{clr|1|j.C}} > {{clr|3|j.214D}} > {{clr|5|2A}}x5 > {{clr|3|c.D}}(1) > A+C > {{clr|5|j.A}} > {{clr|3|j.214D}} > {{clr|5|2A}}x15 > {{clr|4|2B}} > {{clr|3|5D}} > {{clr|3|214D}}+E > {{clr|5|2A}}x14 > {{clr|5|2A}}~E > {{clr|5|2A}}x4 > {{clr|4|2B}} > {{clr|3|5D}} > {{clr|3|214D}}+E > {{clr|5|2A}}x10 > {{clr|4|2B}} > {{clr|1|c.C}} > C+D > 6 > {{clr|4|2B}} > {{clr|5|2A}}x5 > {{clr|5|2A}}~E > {{clr|5|2A}}x14 > {{clr|5|A}}xN~ (Traveling)
| content    =
An optimal, 5-hit-bani combo which has been been fine-tuned to balance ease of use with Boost recovery while guaranteeing a ToD.

Like all optimal bani routes, this combo assumes that you have no starting resources and start the combo with a grounded hit into 5-hit-bani. You can still route into something similar to this by using the standard combo pieces of {{Keyword|Ura Sai}} > {{Keyword|Renda Sai}}. The advantage of this route over that kind of combo is that the less-optimal routes involve more TK Sais and take much longer to start cranking out resources. TK Sai is the most common point to drop a Raoh combo, so minimizing how many are needed is a huge boon to your combo.
}}

==Complex Example==
{{TheoryBox
| Title   = Optimal Banishing Strike Combo | Oneliner   = {{CLabel|HNK|Toki}} variant    
| Anchor  = OptimalTokiBani                | Difficulty = {{clr|5|Extremely Hard}}
| Video   = HNK Raoh Placeholder Video.mp4 | Size       = 256x192
| Recipe  = {{clr|5|2A}}x2 > {{clr|4|2B}} > {{clr|3|5D}} > C+D > 6 > {{clr|1|2C}} > slight delay 9E > {{clr|5|j.A}} > {{clr|4|j.B}} > {{clr|3|j.D}} > land then jump straight up > {{clr|5|j.A}} (right side only) > {{clr|3|j.214D}} > {{clr|5|2A}}x12 > {{clr|3|c.D}}(1) > A+C > {{clr|1|j.C}} > {{clr|3|j.214D}} > {{clr|5|2A}}x5 > {{clr|3|c.D}}(1) > A+C > {{clr|5|j.A}} > {{clr|3|j.214D}} > {{clr|5|2A}}x15 > {{clr|4|2B}} > {{clr|3|5D}} > {{clr|3|214D}}+E > {{clr|5|2A}}x14 > {{clr|5|2A}}~E > {{clr|5|2A}}x4 > {{clr|4|2B}} > {{clr|3|5D}} > {{clr|3|214D}}+E > {{clr|5|2A}}x10 > {{clr|4|2B}} > {{clr|1|c.C}} > C+D > 6 > {{clr|4|2B}} > {{clr|5|2A}}x5 > {{clr|5|2A}}~E > {{clr|5|2A}}x14 > {{clr|5|A}}xN~ (Traveling)
| content =
An optimal, 5-hit-bani combo which has been been fine-tuned to balance ease of use with Boost recovery while guaranteeing a ToD.

Like all optimal bani routes, this combo assumes that you have no starting resources and start the combo with a grounded hit into 5-hit-bani. You can still route into something similar to this by using the standard combo pieces of {{Keyword|Ura Sai}} > {{Keyword|Renda Sai}}. The advantage of this route over that kind of combo is that the less-optimal routes involve more TK Sais and take much longer to start cranking out resources. TK Sai is the most common point to drop a Raoh combo, so minimizing how many are needed is a huge boon to your combo.
}}
```

again, I think this is quite readable, and will probably go with the 3 line version.
