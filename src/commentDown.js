const vscode  = require('vscode');

const { executeCommand } = vscode.commands;

/**
 * duplicates a line as a comment above itself.
 *
 * @date 9/23/2023 - 7:22:47 PM
 */
const commentDown = function commentDown() {
	executeCommand('editor.action.copyLinesDownAction');
	executeCommand('cursorUp');
	executeCommand('editor.action.addCommentLine');
	executeCommand('cursorDown');
	return true;
};

module.exports = { commentDown };
