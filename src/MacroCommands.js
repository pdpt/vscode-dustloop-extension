/**
 * Helper class to make creating the module.exports.macroCommands object easier.
 *
 * @date 9/23/2023 - 7:20:21 PM
 *
 * @class MacroCommands
 * @typedef {MacroCommands}
 */
class MacroCommands {
	static object = {};

	constructor(...args) {
		MacroCommands.add(...args);
	}

	static add(...args) {
		[...args].forEach((item) => {
			MacroCommands.object[item.name] = {
				no   : MacroCommands.getIndex(),
				func : item,
			};
		});
	}

	static getIndex() {
		const entries = Object.entries(MacroCommands.object);
		return entries.length;
	}

	static getObject() {
		return MacroCommands.object;
	}
}

module.exports = { MacroCommands };
