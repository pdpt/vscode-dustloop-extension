// readCombos.js;
const vscode  = require('vscode');
const { assert, Util } = require('./utils');
const { ComboObject } = require('./wikiObjects/ComboObject');

const HEADERS = ['GGST-ComboTableRow', 'TheoryBox'];

/**
 * macro to read a combo from a theorybox or comboTableRow
 */

/* eslint-disable max-len */
const testString = `

{{TheoryBox
	| Title      = Beginner Ground Combo
	| Oneliner   = Works against any grounded opponent.
	| Difficulty = {{clr|1|Very Easy}}
	| Anchor     = BeginnerCombo1
	| Video      = GGACR Venom Beginner BNB.webm
	| Size       = 256x192
	| Recipe     = Starter > {{clr|3|c.S}}(3) > {{clr|2|6K}} > {{clr|2|421K}}~{{clr|5|D}}
	| content    =
	This is an easy way to get damage as Venom while setting up his high/low okizeme, and acts as a foundational component for his core combos.
	
	Common starters include {{clr|2|2K}}, {{MMC|game=GGACR|chara=Venom|input=j.236S|label={{clr|3|j.236S}}}}, and {{clr|4|5H}} Stagger, but you can route into this combo any time you are close enough to connect the 3rd hit of {{clr|3|c.S}} against a grounded opponent. If you are too far for the 3rd hit to connect, then replace {{clr|2|6K}} with {{clr|4|5H}}.
	
	By routing into {{MMC|game=GGACR|input=421X|chara=Venom|label=Dubious Curve}}—(here,{{clr|2|421K}}~{{clr|5|D}})—you set up a multi-hit projectile that can be used for the standard K-Ball Okizeme. If you desire a different, but similarly easy okizeme setup, then you can cancel into a different version of Dubious Curve. These include {{clr|1|421P}} for P-Ball Okizeme, and {{clr|4|421H}} for H-Ball Okizeme.
}}


`;
/* eslint-enable max-len */

const test = Util.findSurroundedBy(testString, 0);
console.log('[', testString.slice(test.start, test.end), ']');

/**
 * get an array of strings that start & end with `{{`, `}}`
 * todo: this is probably removable, this whole file too.
 * @date 9/27/2023 - 10:06:51 PM
 */
const getObjectArray = function getObjectArray() {
	const editor = vscode.window.activeTextEditor;
	const { document, selection } = editor;
	assert(editor, 'Editor is not opening');

	const comboArray = [];
	const string = document.getText(selection) || document.getText();

	let matches = [];
	// get a list of all instances of `{{<prefix>`
	HEADERS.forEach((header) => {
		// eslint-disable-next-line security/detect-non-literal-regexp
		const regex = new RegExp(`{{${header}`, 'gdim');
		const result = regex.exec(string);
		const map = result?.indices?.map((value) => value[0]);
		matches = [...matches, ...(map || [])];
	});

	matches.forEach((start) => {
		// start a recursive function to find the end of the object.
		const end = Util.findSurroundedBy(string, start);
		const slice = string.slice(start, end);
		const comboObject = ComboObject.fromWiki(slice);

		comboArray.push(comboObject);
	});

	return comboArray;
};

const readCombos = function readComboFromWiki() {
	console.clear();

	const objectArray = getObjectArray();

	objectArray.forEach((object) => {
		console.log(object);
	});
};

module.exports = { readCombos };
