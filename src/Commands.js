/*
 * @file        	Commands
 * @fileoverview	class containing all the commands the extension can execute.
 * @see         	<<filelink>>
 *
 * @author	pdpt
 * @date  	2023-10-1
 */

// const { ComboSetting } = require('vscode-extension-tester');
const { VSCodeHelper } = require('./VSCodeHelper');
const { Color } = require('./wikiObjects/Color');
const { ComboObject } = require('./wikiObjects/ComboObject');

class Commands {
	/**
	 * @description
	 *   adds color to either the selection or full document.
	 *
	 * @date 2023-10-01
	 * @static
	 * @memberof Commands
	 */
	static addColor() {
		/** @type {function(string)} */
		const replaceFunction = Color.colorizeWikiText;
		VSCodeHelper.replaceCommand(replaceFunction);
	}

	/**
	 * @description
	 *   removes color from either the selection or full document.
	 *
	 * @date 2023-10-02
	 * @static
	 * @memberof Commands
	 */
	static removeColor() {
		const replaceFunction = Color.uncolorizeWikiText;
		VSCodeHelper.replaceCommand(replaceFunction);
	}

	/**
	 * @description
	 *   converts color objects to hex format
	 * * {{clr|P|5P}} => {{clr|#123456|5P}}
	 * @date 2023-10-02
	 * @static
	 * @memberof Commands
	 */
	static colorToHex() {
		const replaceFunction = Color.wikiColorsToHex;
		VSCodeHelper.replaceCommand(replaceFunction);
	}

	/**
	 * @description
	 *   converts color objects to button format
	 * * {{clr|#123456|5P}} => {{clr|P|5P}}
	 * @date 2023-10-02
	 * @static
	 * @memberof Commands
	 */
	static colorToButton() {
		const replaceFunction = Color.wikiColorsToButton;
		VSCodeHelper.replaceCommand(replaceFunction);
	}

	/**
	 * @description
	 *   reads the file or selection, finding all TheoryBoxes or GGSTComboTableRows
	 *   creates a collection of ComboObjects one for each found combo
	 *   creates a GGSTComboTable in a new file containing all the combos
	 *
	 * @date 2023-10-02
	 * @static
	 * @memberof Commands
	 */
	static generateComboTable() {
		// get text from file/selection
		const string = VSCodeHelper.getText();

		// finding all TheoryBoxes or GGSTComboTableRows
		// creates a collection of ComboObjects one for each found combo
		const combos = ComboObject.fromWikiText(string);

		// creates a GGSTComboTable containing all the combos.
		const table = ComboObject.toTable(combos);

		// opens a new file, and pastes the table inside.
		VSCodeHelper.stringInNewFile(table);
	}
}

module.exports = { Commands };
