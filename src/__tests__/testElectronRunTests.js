/* eslint-disable unicorn/no-process-exit */
const path = require('node:path');

const { runTests } = require('@vscode/test-electron');

/**
 * uses @vscode/test-electron to open a new instance of vscode for testing,
 * then runs the index.js file in `extensionTestsPath`
 *
 * @date 9/29/2023 - 1:25:36 PM
 *
 * @async
 * @returns {*}
 */
const main = async function main(
	// The folder containing the Extension Manifest package.json
	// Passed to `--extensionDevelopmentPath`
	extensionDevelopmentPath = path.resolve(__dirname, '../../'),
	// The path to the extension test script
	// Passed to --extensionTestsPath
	extensionTestsPath = path.resolve(__dirname, './suite/index'),
) {
	try {
		// const extensionDevelopmentPath = path.resolve(__dirname, '../../');
		// const extensionTestsPath = path.resolve(__dirname, './suite/index');
		// Download VS Code, unzip it and run the integration test
		await runTests({ extensionDevelopmentPath, extensionTestsPath });
	} catch (error) {
		console.error('Failed to run tests', error);
		process.exit(1);
	}
};

// eslint-disable-next-line unicorn/prefer-top-level-await
// main();

module.exports = { main };
