/* eslint-disable no-unused-vars */
const path = require('node:path');

const exTesterRunTests     = require('./exTesterRunTests');
const testElectronRunTests = require('./testElectronRunTests');

/**
 * uses @vscode/test-electron to open a new instance of vscode for testing,
 * then runs the index.js file in `extensionTestsPath`
 *
 * @date 9/29/2023 - 1:25:36 PM
 *
 * @async
 * @returns {*}
 */
const main = async function main() {
	try {
		// The folder containing the Extension Manifest package.json
		// Passed to `--extensionDevelopmentPath`
		const extensionDevelopmentPath = path.resolve(__dirname, '../../');

		// The path to the extension test script
		// Passed to --extensionTestsPath
		const extensionTestsPath = path.resolve(__dirname, './suite/index');

		// Download VS Code, unzip it and run the integration test
		// await runTests({ extensionDevelopmentPath, extensionTestsPath });

		// pass the arguments to each tester.
		// exTesterRunTests.main({ extensionDevelopmentPath, extensionTestsPath });
		testElectronRunTests.main({ extensionDevelopmentPath, extensionTestsPath });
	} catch (error) {
		console.error('Failed to run tests', error);
		// eslint-disable-next-line unicorn/no-process-exit
		process.exit(1);
	}
};

// eslint-disable-next-line unicorn/prefer-top-level-await
main();
