const path = require('node:path');
const Mocha = require('mocha');
const glob = require('glob');

const run = function run() {
	// Create the mocha test
	const mocha = new Mocha({
		ui    : 'bdd',
		color : true,
	});

	// points to the __tests__ folder
	const testsRoot = path.resolve(__dirname, '..');

	return new Promise((onComplete, onError) => {
		// find all `.test.js` files in testsRoot
		const testFiles = new glob.Glob('**/**.test.js', { cwd: testsRoot });
		const testFileStream = testFiles.stream();
		const files = [];

		testFileStream.on('data', (file) => {
			files.push(file);
			// Add files to the test suite
			mocha.addFile(path.resolve(testsRoot, file));
		});
		testFileStream.on('error', (error) => {
			onError(error);
		});
		testFileStream.on('end', () => {
			console.log('files :>>', files);
			try {
				// Run the mocha test
				mocha.run((failures) => {
					if (failures > 0) {
						onError(new Error(`${failures} tests failed.`));
					} else {
						onComplete();
					}
				});
			} catch (error) {
				console.error(error);
				onError(error);
			}
		});
	});
};

module.exports = {
	run,
};
