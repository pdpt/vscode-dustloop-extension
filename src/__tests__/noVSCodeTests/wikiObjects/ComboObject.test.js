const fs = require('node:fs');
const path = require('node:path');

const { expect } = require('expect');
const matchers  = require('jest-extended');
const { ComboObject } = require('../../../wikiObjects/ComboObject');

expect.extend(matchers);

/**
	 * constructor
	 * fromWikiText
	 * toTheoryBox
	 * toTableRow
	 */
// path
const getFile = async(filename) => {
	const resolve = path.resolve(__dirname, '../../../../exampleFiles/', filename);
	const fileString = await fs.readFileSync(resolve).toString();
	console.log(fileString);
	return fileString;
};
// const path = './../../../../exampleFiles/CollapsibleComboListWrapper.wikitext';

/* eslint-disable max-len */

// const theoryBox = `
// 	{{TheoryBox
// 		| Title      = Beginner Ground Combo
// 		| Oneliner   = Works against any grounded opponent.
// 		| Difficulty = {{clr|1|Very Easy}}
// 		| Anchor     = BeginnerCombo1
// 		| Video      = GGACR Venom Beginner BNB.webm
// 		| Size       = 256x192
// 		| Recipe     = Starter > {{clr|3|c.S}}(3) > {{clr|2|6K}} > {{clr|2|421K}}~{{clr|5|D}}
// 		| content    =
// This is an easy way to get damage as Venom while setting up his high/low okizeme, and acts as a foundational component for his core combos.

// Common starters include {{clr|2|2K}}, {{MMC|game=GGACR|chara=Venom|input=j.236S|label={{clr|3|j.236S}}}}, and {{clr|4|5H}} Stagger, but you can route into this combo any time you are close enough to connect the 3rd hit of {{clr|3|c.S}} against a grounded opponent. If you are too far for the 3rd hit to connect, then replace {{clr|2|6K}} with {{clr|4|5H}}.

// By routing into {{MMC|game=GGACR|input=421X|chara=Venom|label=Dubious Curve}}—(here,{{clr|2|421K}}~{{clr|5|D}})—you set up a multi-hit projectile that can be used for the standard K-Ball Okizeme. If you desire a different, but similarly easy okizeme setup, then you can cancel into a different version of Dubious Curve. These include {{clr|1|421P}} for P-Ball Okizeme, and {{clr|4|421H}} for H-Ball Okizeme.
// }}
// 	`;
const comboTableRow = `
{{GGST-ComboTableRow
	|combo={{clr|2|5K}} > {{clr|1|6P}} > {{clr|2|214K}}, {{clr|2|5K}} > {{clr|4|6H}} > {{clr|3|623S}} WS {{clr|5|5[D]}} WB
	|position=Corner
	|damage=166
	|worksOn=Everyone
	|difficulty=Medium
	|tensionGain=28
	|notes=Requires the enemy to be close to the corner for {{clr|3|623S}} not to whiff after {{clr|4|6H}}
	|recipePC=
	|checkedVersion=1.29
	}}
`;

const expectedTheoryBox = {
	title      : 'Beginner Ground Combo',
	oneliner   : 'Works against any grounded opponent.',
	difficulty : '{{clr|1|Very Easy}}',
	anchor     : 'BeginnerCombo1',
	video      : 'GGACR Venom Beginner BNB.webm',
	recipe     : 'Starter > {{clr|3|c.S}}(3) > {{clr|2|6K}} > {{clr|2|421K}}~{{clr|5|D}}',
	content    : `
This is an easy way to get damage as Venom while setting up his high/low okizeme, and acts as a foundational component for his core combos.

Common starters include {{clr|2|2K}}, {{MMC|game=GGACR|chara=Venom|input=j.236S|label={{clr|3|j.236S}}}}, and {{clr|4|5H}} Stagger, but you can route into this combo any time you are close enough to connect the 3rd hit of {{clr|3|c.S}} against a grounded opponent. If you are too far for the 3rd hit to connect, then replace {{clr|2|6K}} with {{clr|4|5H}}.

By routing into {{MMC|game=GGACR|input=421X|chara=Venom|label=Dubious Curve}}—(here,{{clr|2|421K}}~{{clr|5|D}})—you set up a multi-hit projectile that can be used for the standard K-Ball Okizeme. If you desire a different, but similarly easy okizeme setup, then you can cancel into a different version of Dubious Curve. These include {{clr|1|421P}} for P-Ball Okizeme, and {{clr|4|421H}} for H-Ball Okizeme.
`.trim(),
};

const testEqual = (actual, expected) => () => {
	expect(actual).toEqual(expected);
};

describe('ComboObject', () => {
	describe('.constructor()',         () => {
		it('should have the correct keys', () => {
			const comboObject = new ComboObject();

			expect(comboObject).toContainKey('combo');
			expect(comboObject).toContainKey('position');
			expect(comboObject).toContainKey('difficulty');
			expect(comboObject).toContainKey('damage');
			expect(comboObject).toContainKey('workson');
			expect(comboObject).toContainKey('tensiongain');
			expect(comboObject).toContainKey('setup');
			expect(comboObject).toContainKey('recipe');
			expect(comboObject).toContainKey('checkedversion');
			expect(comboObject).toContainKey('anchor');
			expect(comboObject).toContainKey('video');
			expect(comboObject).toContainKey('youtube');
			expect(comboObject).toContainKey('timestamp');
			expect(comboObject).toContainKey('autoplay');
			expect(comboObject).toContainKey('title');
			expect(comboObject).toContainKey('oneliner');
			expect(comboObject).toContainKey('notes');
			expect(comboObject).toContainKey('content');
		});
	});
	describe('.getObjectsWithSkips()', () => {});
	describe('.fromWikiText()',        async() => {
		const comboList         = await getFile('CollapsibleComboListWrapper.wikitext');
		const theoryBox         = await getFile('TheoryBox.wikitext');
		const bedman            = await getFile('bedman.wikitext');
		const wikitableSortable = await getFile('wikitableSortable.wikitext');

		it('should parse theoryBox without error', () => {
			expect(() => { ComboObject.fromWikiText(theoryBox); }).not.toThrow();
		});
		it('should parse comboTableRow without error', () => {
			expect(() => { ComboObject.fromWikiText(comboTableRow); }).not.toThrow();
		});
		it('should parse both objects without error', () => {
			expect(() => {
				const x = ComboObject.fromWikiText([comboTableRow, theoryBox].join(''));
				console.log(x);
			}).not.toThrow();
		});
		it('should return an array of length two', () => {
			const comboArray = ComboObject.fromWikiText([comboTableRow, theoryBox].join(''));
			console.log(comboArray);
			expect(comboArray.length).toBe(2);
		});

		it('should have correct keys', () => {
			const actual = ComboObject.fromWikiText(theoryBox)[0];
			console.log(actual);

			expect(actual?.title).toEqual(expectedTheoryBox.title);
			expect(actual?.oneliner).toEqual(expectedTheoryBox.oneliner);
			expect(actual?.difficulty).toEqual(expectedTheoryBox.difficulty);
			expect(actual?.anchor).toEqual(expectedTheoryBox.anchor);
			expect(actual?.video).toEqual(expectedTheoryBox.video);
			expect(actual?.recipe).toEqual(expectedTheoryBox.recipe);
			const actualSplit = actual.content.split('\n');
			const expectedSplit = expectedTheoryBox.content.split('\n');
			expectedSplit.forEach((item, index) => {
				expect(actualSplit[index].trim()).toEqual(expectedSplit[index].trim());
			});
		});

		it('should read a CollapsibleComboListWrapper', () => {
			const comboArray = ComboObject.fromWikiText(comboList);
			console.log('comboArray.length :>>', comboArray.length);
			expect(comboArray.length).toBe(7);
		});

		it('should get all values from wikitableSortable', () => {
			const combo = ComboObject.fromWikiText(wikitableSortable).pop();
			const expected = {
				combo          : '(66) {{clr|1|5P}}/{{clr|1|2P}}/{{clr|2|5K}}/{{clr|2|2K}} > {{clr|1|6P}} (> {{clr|2|214K}})',
				position       : 'Anywhere',
				damage         : '70 (73) [76]',
				workson        : 'Everyone',
				difficulty     : 'Very Easy',
				notes          : 'Close range {{clr|1|P}}/{{clr|2|K}} confirm. {{clr|1|6P}} > {{clr|2|214K}} is guaranteed in the corner, but very close range or dash momentum is needed midscreen for the combo to connect midscreen. Damage given for {{clr|1|5P}} starter.',
				recipe         : '52491',
				checkedversion : '1.19',
			};
			expect(combo.combo).toEqual(expected.combo);
			expect(combo.position).toEqual(expected.position);
			expect(combo.damage).toEqual(expected.damage);
			expect(combo.workson).toEqual(expected.workson);
			expect(combo.difficulty).toEqual(expected.difficulty);
			expect(combo.notes).toEqual(expected.notes);
			expect(combo.recipe).toEqual(expected.recipe);
			expect(combo.checkedversion).toEqual(expected.checkedversion);
		});

		it('should generate a table equivalent to the input', () => {
			const comboArray  = ComboObject.fromWikiText(wikitableSortable);
			const tableString = ComboObject.toTable(comboArray);
			const actualSplit = tableString.split(/\s+/gim);
			const expectedSplit = wikitableSortable.split(/\s+/gim);
			expectedSplit.forEach((item, index) => {
				// eslint-disable-next-line security/detect-object-injection
				expect(actualSplit[index]).toEqual(expectedSplit[index]);
			});
			// expect(actualSplit).toEqual(expectedSplit);
			return tableString;
		});
	});
	describe('.toTheoryBox()',         () => {});
	describe('.toTableRow()',          () => {});
});
