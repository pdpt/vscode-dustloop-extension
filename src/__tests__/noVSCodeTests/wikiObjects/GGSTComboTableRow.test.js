// const assert = require('node:assert');
// const util = require('node:util');
// import expect from 'expect'
const { expect } = require('expect');
// const { expect, jest, test } = require('@jest/globals');

// You can import and use all API from the 'vscode' module
// as well as import your extension to test it
// eslint-disable-next-line import/no-unresolved
// const vscode = require('vscode');
// const myExtension = require('../extension');

const { GGSTComboTableRow } = require('../../../wikiObjects/GGSTComboTableRow');

describe('GGSTComboTableRow', () => {
	/* eslint-disable max-len */

	const exampleString = `
	{{GGST-ComboTableRow
		|combo = (66) {{clr|1|5P}}/{{clr|1|2P}}/{{clr|2|5K}}/{{clr|2|2K}} > {{clr|1|6P}} (> {{clr|2|214K}})
		|position = Anywhere
		|damage = 70 (73) [76]
		|tensionGain = ~14
		|worksOn = Everyone
		|difficulty = Very Easy
		|notes = Close range {{clr|1|P}}/{{clr|2|K}} confirm. {{clr|1|6P}} > {{clr|2|214K}} is guaranteed in the corner, but very close range or dash momentum is needed midscreen for the combo to connect midscreen. Damage given for {{clr|1|5P}} starter.
		|recipePC = 52491
		|recipePS = 35854
		|checkedVersion = 1.19
	}}`;

	const expected = {
		combo          : '(66) {{clr|1|5P}}/{{clr|1|2P}}/{{clr|2|5K}}/{{clr|2|2K}} > {{clr|1|6P}} (> {{clr|2|214K}})',
		position       : 'Anywhere',
		damage         : '70 (73) [76]',
		tensiongain    : '~14',
		workson        : 'Everyone',
		difficulty     : 'Very Easy',
		notes          : 'Close range {{clr|1|P}}/{{clr|2|K}} confirm. {{clr|1|6P}} > {{clr|2|214K}} is guaranteed in the corner, but very close range or dash momentum is needed midscreen for the combo to connect midscreen. Damage given for {{clr|1|5P}} starter.',
		recipe         : '52491',
		checkedversion : '1.19',
	};
	/* eslint-enable max-len */

	const toEqual = (act, exp) => () => { expect(act).toEqual(exp, `${act} !== ${exp}`); };

	describe('static .fromString()', () => {
		const actual = GGSTComboTableRow.fromString(exampleString);

		it('combo          should be equal', toEqual(actual?.combo, expected.combo));
		it('position       should be equal', toEqual(actual?.position, expected.position));
		it('damage         should be equal', toEqual(actual?.damage, expected.damage));
		it('tensiongain    should be equal', toEqual(actual?.tensiongain, expected.tensiongain));
		it('workson        should be equal', toEqual(actual?.workson, expected.workson));
		it('difficulty     should be equal', toEqual(actual?.difficulty, expected.difficulty));
		it('notes          should be equal', toEqual(actual?.notes, expected.notes));
		it('recipe         should be equal', toEqual(actual?.recipe, expected.recipe));
		it('checkedversion should be equal', toEqual(actual?.checkedversion, expected.checkedversion));

		it('should deep equal the expected', () => {
			expect(actual).toEqual(expected);
		});

		// const expectedArray = Object.entries(expected);
		// const x = util.format('%s:%s', expectedArray[0][0], expectedArray[0][1]);
		// describe.each(expectedArray)(
		// 	'%#: %s should be %s',
		// 	(key, value) => {
		// 		test(`returns ${value}`, () => {
		// 			const actual = actual?.[key];

		// 			// assert.equal(actual, value, `${actual} !== ${value}`);
		// 			expect(actual).toBe(value);
		// 		});
		// 	},
		// );

		// Object.entries(expectedArray).forEach((entry) => {
		// 	const [key, value] = entry;
		// 	it(`${key} should be ${value}`, () => {
		// 		// eslint-disable-next-line security/detect-object-injection
		// 		const actual = comboTableRow?.[key];

		// 		assert.equal(actual, value, `${actual} !== ${value}`);
		// 	});
		// });
		// it('should equal expected', () => {
		// 	assert.deepEqual(comboTableRow, expected);
		// });
		it('combo: (66) {{clr|1|5P}}/{{clr|1|2P}}/{{clr|2|5...', () => {
			const { combo } = actual;
			const { combo: exCombo } = expected;
			expect(combo).toBe(exCombo);
		});
		it('should have position: Anywhere', () => {
			const { position } = actual;
			const { position: exPosition } = expected;
			expect(position).toBe(exPosition);
		});
		it('should have damage: Anywhere', () => {
			const { damage } = actual;
			const { damage: exDamage } = expected;
			expect(damage).toBe(exDamage);
		});
	});
	describe('constructor', () => {});
	describe('.toString()', () => {});
	describe('.getWikiText()', () => {});
	describe('.getWikiTextCollapsed()', () => {});
});
