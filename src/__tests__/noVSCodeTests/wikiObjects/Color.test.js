// const assert = require('node:assert');
const { expect } = require('expect');

// You can import and use all API from the 'vscode' module
// as well as import your extension to test it
// const vscode = require('vscode');
const assert = require('node:assert');
const { Color } = require('../../../wikiObjects/Color');
// const { RegexCollection } = require('../../../RegexCollection');
// const myExtension = require('../extension');

const toBe = (input, expected, getActual) => () => {
	const actual = getActual(input);
	expect(actual).toBe(expected, `${input} !=> ${expected}`);
	assert.strictEqual(actual, expected);
};

describe('Color', () => {
	describe('.colorWord()', () => {
		const colorWordToBe = (input, expected) => toBe(input, expected, Color.colorWord);

		const tests = [
			['5P',       '{{clr|P|5P}}'],
			['!H',       '{{clr|H|!H}}'],
			['j.63214S', '{{clr|S|j.63214S}}'],
		];
		it(`should ${tests[0][0]} => ${tests[0][1]}`, colorWordToBe(tests[0][0], tests[0][1]));
		it(`should ${tests[1][0]} => ${tests[1][1]}`, colorWordToBe(tests[1][0], tests[1][1]));
		it(`should ${tests[2][0]} => ${tests[2][1]}`, colorWordToBe(tests[2][0], tests[2][1]));
	});
	describe('.convertCodeToType()', () => {
		const tests = [
			{ input: { code: 'Very Easy', type: Color.KEY.number     }, expected: 1 },
			{ input: { code: '#1ba6ff',   type: Color.KEY.button     }, expected: 'K' },
			{ input: { code: 'S',         type: Color.KEY.hex        }, expected: '#16df53' },
			{ input: { code: 4,           type: Color.KEY.difficulty }, expected: 'Hard' },
			{ input: { code: 'Very Hard', type: Color.KEY.button     }, expected: 'D' },
		];
		const getActual = Color.convertCodeToType;
		tests.forEach(({ input, expected }) => {
			it(`should {${input.code}, ${input.type}} => ${expected}`, () => {
				const { code, type } = input;
				const actual = getActual(code, type);
				expect(actual).toBe(expected);
			});
		});
	});
	describe('.uncolorWord()', () => {
		const tests = [
			{ input: '{{clr|P|5P}}',       expected: '5P'       },
			{ input: '{{clr|H|!H}}',       expected: '!H'       },
			{ input: '{{clr|S|j.63214S}}', expected: 'j.63214S' },
		];
		const getActual = Color.uncolorWord;
		tests.forEach(({ input, expected }) => {
			it(`should ${input} => ${expected}`, () => {
				const actual = getActual(input);
				expect(actual).toBe(expected, `${input} !=> ${expected}`);
			});
		});
	});
	describe('.getCodeFromWord()', () => {
		const tests = [
			{ input: '5P', expected: 'P' },
			{ input: 'Easy', expected: 'Easy' },
			{ input: 'c.S', expected: 'S' },
			{ input: 3, expected: 3 },
			{ input: '#e8982c', expected: '#e8982c' },
			{ input: 'j.632146S', expected: 'S' },
		];
		const getActual = Color.getCodeFromWord;
		tests.forEach(({ input, expected }) => {
			it(`should ${input} => ${expected}`, () => {
				const actual = getActual(input);
				expect(actual).toBe(expected, `${input} !=> ${expected}`);
			});
		});
	});
	describe('.codeTo*() Helpers', () => {
		const tests = [
			// codeToNumber
			{ input: 'Very Easy', expected: 1,         getActual: Color.codeToNumber },
			// codeToButton
			{ input: '#1ba6ff',   expected: 'K',       getActual: Color.codeToButton },
			// codeToHex
			{ input: 'S',         expected: '#16df53', getActual: Color.codeToHex },
			// codeToDifficulty
			{ input: 4,           expected: 'Hard',    getActual: Color.codeToDifficulty },
		];
		tests.forEach(({ input, expected, getActual }) => {
			it(`should ${input}=>${expected}`, () => {
				const actual = getActual(input);
				expect(actual).toBe(expected);
			});
		});
	});
	describe('.findUncoloredWord()', () => {
		const tests = [
			{ input: 'test 5K ',              expected: '5K' },
			{ input: 'test !H ',              expected: '!H' },
			{ input: 'test 236{H} ',          expected: '236{H}' },
			{ input: 'test 236[H] ',          expected: '236[H]' },
			{ input: '6P test',               expected: '6P' },
			{ input: '{{clr|S|5S}} 214K 5K ', expected: '214K' },
			{ input: '6H ',                   expected: '6H' },
			{ input: '623S WS 5[D] ',         expected: '623S' },
			{ input: 'c.S j.632146S',         expected: 'c.S' },
			{ input: 'j.632146S test c.S ',   expected: 'j.632146S' },
		];
		const getActual = Color.findUncoloredWord;
		tests.forEach(({ input, expected }) => {
			it(`should find the first colorable word: ${expected}`, () => {
				const expectedStart = input.indexOf(expected);
				const expectedEnd   = expectedStart + expected.length;
				const actual        = getActual(input);
				expect(actual.start).toBe(expectedStart);
				expect(actual.end).toBe(expectedEnd);
				// expect(actual).toBe( expected, `${input} !=> ${expected}`)
			});
		});
	});
	describe('.colorizeString()', () => {
		const tests = [
			{ input: 'test 5K ',              expected: 'test {{clr|K|5K}} ' },
			{ input: 'test !H ',              expected: 'test {{clr|H|!H}} ' },
			{ input: 'test 236{H} ',          expected: 'test {{clr|H|236{H}}} ' },
			{ input: 'test 236[H] ',          expected: 'test {{clr|H|236[H]}} ' },
			{ input: '6P test',               expected: '{{clr|P|6P}} test' },
			{ input: '{{clr|S|5S}} 214K 5K ', expected: '{{clr|S|5S}} {{clr|K|214K}} {{clr|K|5K}} ' },
			{ input: '6H ',                   expected: '{{clr|H|6H}} ' },
			{ input: '623S WS 5[D] ',         expected: '{{clr|S|623S}} WS {{clr|D|5[D]}} ' },
			{ input: 'c.S j.632146S',         expected: '{{clr|S|c.S}} {{clr|S|j.632146S}}' },
			{ input: 'j.632146S test c.S ',   expected: '{{clr|S|j.632146S}} test {{clr|S|c.S}} ' },
		];
		const getActual = Color.colorizeString;
		tests.forEach(({ input, expected }) => {
			it(`should ${input} => ${expected}`, () => {
				const actual = getActual(input);
				expect(actual).toBe(expected, `${input} !=> ${expected}`);
			});
		});
	});
	describe('.colorizeWithSkips()', () => {
		const tests = [
			{ input: '<!-- -->test 5K ',             expected: '<!-- -->test {{clr|K|5K}} ' },
			{ input: '<!-- -->test !H ',             expected: '<!-- -->test {{clr|H|!H}} ' },
			{ input: '<!-- -->test 236{H} <!-- -->', expected: '<!-- -->test {{clr|H|236{H}}} <!-- -->' },
			{ input: '<!-- test 236[H] -->',         expected: '<!-- test 236[H] -->' },
			{ input: '<!--6P test -->',              expected: '<!--6P test -->' },
			{ input: '<tabber>\n214K 5K=\n ',        expected: '<tabber>\n214K 5K=\n ' },
			{ input: '<tabber>\n623S WS=\n 5[D] ',   expected: '<tabber>\n623S WS=\n {{clr|D|5[D]}} ' },
			{ input    : '<!-- -->\ntest 236{H} \n<!-- -->',
				expected : '<!-- -->\ntest {{clr|H|236{H}}} \n<!-- -->' },
		];
		const getActual = Color.colorizeWithSkips;
		tests.forEach(({ input, expected }) => {
			it(`should ${input} => ${expected}`, () => {
				const actual = getActual(input);
				expect(actual).toBe(expected, `${input} !=> ${expected}`);
			});
		});
	});
	describe('.colorizeWikiText()', () => {
		const tests = [
			{ input: '<!-- -->test 5K ',             expected: '<!-- -->test {{clr|K|5K}} ' },
			{ input: '<!-- -->test !H ',             expected: '<!-- -->test {{clr|H|!H}} ' },
			{ input: '<!-- -->test 236{H} <!-- -->', expected: '<!-- -->test {{clr|H|236{H}}} <!-- -->' },
			{ input: '<!-- test 236[H] -->',         expected: '<!-- test 236[H] -->' },
			{ input: '<!--6P test -->',              expected: '<!--6P test -->' },
			{ input: '<tabber>\n214K 5K=\n ',        expected: '<tabber>\n214K 5K=\n ' },
			{ input: '<tabber>\n623S WS=\n 5[D] ',   expected: '<tabber>\n623S WS=\n {{clr|D|5[D]}} ' },
			{ input    : '<!-- -->\ntest 236{H} \n<!-- -->',
				expected : '<!-- -->\ntest {{clr|H|236{H}}} \n<!-- -->' },
		];
		const getActual = Color.colorizeWikiText;
		tests.forEach(({ input, expected }) => {
			it(`should ${input} => ${expected}`, () => {
				const actual = getActual(input);
				expect(actual).toBe(expected, `${input} !=> ${expected}`);
			});
		});
	});
	describe('.uncolorizeWikiText()', () => {
		const tests = [
			{ input: '<!-- -->test {{clr|K|5K}} ',             expected: '<!-- -->test 5K ' },
			{ input: '<!--  {{clr|K|5K}}  -->test',            expected: '<!--  {{clr|K|5K}}  -->test' },
			{ input: '<!-- -->test {{clr|H|!H}} ',             expected: '<!-- -->test !H ' },
			{ input: '<!-- -->test {{clr|H|236{H}}} <!-- -->', expected: '<!-- -->test 236{H} <!-- -->' },
			{ input: '<!-- test 236[H] -->',                   expected: '<!-- test 236[H] -->' },
			{ input: '<!--6P test -->',                        expected: '<!--6P test -->' },
			{ input: '<tabber>\n214K 5K=\n ',                  expected: '<tabber>\n214K 5K=\n ' },
			{ input: '<tabber>\n623S WS=\n {{clr|D|5[D]}} ',   expected: '<tabber>\n623S WS=\n 5[D] ' },
			{ input    : '<!-- -->\ntest {{clr|H|236{H}}} \n<!-- -->',
				expected : '<!-- -->\ntest 236{H} \n<!-- -->' },
		];
		const getActual = Color.uncolorizeWikiText;
		tests.forEach(({ input, expected }) => {
			it(`should ${input} => ${expected}`, () => {
				const actual = getActual(input);
				expect(actual).toBe(expected, `${input} !=> ${expected}`);
			});
		});
	});
	describe('.wikiColorsToHex()', () => {
		const tests = [

			/* eslint-disable max-len */

			{ input: '<!-- -->test {{clr|Easy|5K}} ',                expected: '<!-- -->test {{clr|#1ba6ff|5K}} ' },
			{ input: '<!--  {{clr|Easy|5K}}  -->test',               expected: '<!--  {{clr|Easy|5K}}  -->test' },
			{ input: '<!-- -->test {{clr|#d96aca|!P}} ',             expected: '<!-- -->test {{clr|#d96aca|!P}} ' },
			{ input: '<!-- -->test {{clr|#ff0000|236{H}}} <!-- -->', expected: '<!-- -->test {{clr|#ff0000|236{H}}} <!-- -->' },
			{ input: '<!-- test 236[H] -->',                         expected: '<!-- test 236[H] -->' },
			{ input: '<!--6P test -->',                              expected: '<!--6P test -->' },
			{ input: '<tabber>\n214K 5K=\n ',                        expected: '<tabber>\n214K 5K=\n ' },
			{ input: '<tabber>\n623S WS=\n {{clr|5|5[D]}} ',         expected: '<tabber>\n623S WS=\n {{clr|#e8982c|5[D]}} ' },
			{ input    : '<!-- -->\ntest {{clr|H|236{H}}} \n<!-- -->',
				expected : '<!-- -->\ntest {{clr|#ff0000|236{H}}} \n<!-- -->' },
			/* eslint-enable max-len */

		];
		const getActual = Color.wikiColorsToHex;
		tests.forEach(({ input, expected }) => {
			it(`should ${input} => ${expected}`, () => {
				const actual = getActual(input);
				expect(actual).toBe(expected, `${actual} !== ${expected}`);
			});
		});
	});
	describe('.wikiColorsToButton()', () => {
		const tests = [
			/* eslint-disable max-len */

			{ input: '<!-- -->test {{clr|Easy|5K}} ',                expected: '<!-- -->test {{clr|K|5K}} ' },
			{ input: '<!--  {{clr|Easy|5K}}  -->test',               expected: '<!--  {{clr|Easy|5K}}  -->test' },
			{ input: '<!-- -->test {{clr|#d96aca|!P}} ',             expected: '<!-- -->test {{clr|P|!P}} ' },
			{ input: '<!-- -->test {{clr|#ff0000|236{H}}} <!-- -->', expected: '<!-- -->test {{clr|H|236{H}}} <!-- -->' },
			{ input: '<!-- test 236[H] -->',                         expected: '<!-- test 236[H] -->' },
			{ input: '<!--6P test -->',                              expected: '<!--6P test -->' },
			{ input: '<tabber>\n214K 5K=\n ',                        expected: '<tabber>\n214K 5K=\n ' },
			{ input: '<tabber>\n623S WS=\n {{clr|5|5[D]}} ',         expected: '<tabber>\n623S WS=\n {{clr|D|5[D]}} ' },
			{ input    : '<!-- -->\ntest {{clr|H|236{H}}} \n<!-- -->',
				expected : '<!-- -->\ntest {{clr|H|236{H}}} \n<!-- -->' },

			/* eslint-enable max-len */

		];
		const getActual = Color.wikiColorsToButton;
		tests.forEach(({ input, expected }) => {
			it(`should ${input} => ${expected}`, () => {
				const actual = getActual(input);
				expect(actual).toBe(expected, `${actual} !== ${expected}`);
			});
		});
	});
});
