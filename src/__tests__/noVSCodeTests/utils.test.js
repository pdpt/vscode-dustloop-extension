const { expect } = require('expect');
const matchers  = require('jest-extended');

expect.extend(matchers);

const { Util } = require('../../utils');
const { RegexCollection } = require('../../RegexCollection');

const { isBetween, findSurroundedBy, splitWithSkips } = Util;

describe('Util', () => {
	describe('.ifMessage()', () => {
		it('should return the boolean of the first argument', () => {
			const actualTrue = Util.ifMessage(true);
			expect(actualTrue).toBe(true);
			const actualFalse = Util.ifMessage(false);
			expect(actualFalse).toBe(false);
		});
	});
	describe('.assert()', () => {
		it('returns true if first argument is true', () => {
			const argument = true;
			const assertion = Util.assert(argument);
			console.log(assertion);
			expect(assertion).toBe(true);
		});
		it('throws an error if first argument is false', () => {
			const argument = false;
			console.log(argument);

			expect(() => {
				Util.assert(argument);
			}).toThrow();
		});
	});
	describe('.warn()', () => {});
	describe('.assertType()', () => {});
	describe('.isArray()', () => {});
	describe('.getRegexIndexes()', () => {});
	describe('.getSplitString()', () => {
		it('should skip the string "Anywhere="', () => {
			const input = `
			<tabber>
			Anywhere=
			{| class="wikitable sortable"
			`;
			// const skipString = 'Anywhere=';
			// eslint-disable-next-line unicorn/better-regex
			const skipString  = input.match(/\n(.*=\s*)\n/dgm)?.[0];
			const start       = input.indexOf(skipString);
			const end         = start + skipString.length;
			const expected    = { start, end };
			const skipRegex   = RegexCollection.doNotFormat();
			const actual      = Util.getSplitString(input, skipRegex)[1];
			const actualStart = input.indexOf(actual);
			const actualEnd   = actualStart + actual.length;
			expect(actualStart).toBe(expected.start);
			expect(actualEnd).toBe(expected.end);
		});
	});
	describe('.isBetween()', () => {
		it('5 isBetween 7, 3',       () => { expect(isBetween(7, 5, 3)); });
		it(' 01: -986,  156,   255', () => { expect(isBetween(-986,  156, 255)); });
		it(' 02: -920, -799,    49', () => { expect(isBetween(-920, -799,  49)); });
		it(' 03: -463,  -91,   136', () => { expect(isBetween(-463,  -91, 136)); });
		it(' 04:  -36,  408,   932', () => { expect(isBetween(-36,  408,  932)); });
		it(' 05: -476,  -83,   753', () => { expect(isBetween(-476,  -83, 753)); });
		it(' 06: -902, -606,    52', () => { expect(isBetween(-902, -606,  52)); });
		it(' 07: -491,  912,   987', () => { expect(isBetween(-491,  912, 987)); });
		it(' 08: -433, -425,    -5', () => { expect(isBetween(-433, -425,  -5)); });
		it(' 09: -355,  136,   586', () => { expect(isBetween(-355,  136, 586)); });
		it(' 10: -217,  727,   997', () => { expect(isBetween(-217,  727, 997)); });
		it('!11:  163, -516, -129,', () => { expect(isBetween(163, -516, -129)).toBeFalsy(); });
		it('!12:  929, -593,   51,', () => { expect(isBetween(929, -593,   51)).toBeFalsy(); });
		it('!13:  -24, -998, -261,', () => { expect(isBetween(-24, -998, -261)).toBeFalsy(); });
		it('!14:  980, -476, -344,', () => { expect(isBetween(980, -476, -344)).toBeFalsy(); });
		it('!15:  342, -847, -148,', () => { expect(isBetween(342, -847, -148)).toBeFalsy(); });
		it('!16:  399, -236,  -78,', () => { expect(isBetween(399, -236,  -78)).toBeFalsy(); });
		it('!17:  401,  709, -542,', () => { expect(isBetween(401,  709, -542)).toBeFalsy(); });
		it('!18:  307,  431, -933,', () => { expect(isBetween(307,  431, -933)).toBeFalsy(); });
		it('!19:  432,  453, -278,', () => { expect(isBetween(432,  453, -278)).toBeFalsy(); });
		it('!20:  720,  926,  380,', () => { expect(isBetween(720,  926,  380)).toBeFalsy(); });
	});
	describe('.getMatchesFromString()', () => {});
	describe('.removeDupes()', () => {});
	describe('.getEditObject()', () => {});
	describe('.getAllEdits()', () => {});
	describe('.getLineMatches()', () => {});
	describe('.CSVToArray()', () => {});
	describe('.CSVToObject()', () => {});
	describe('.findSurroundedBy()', () => {
		const toBe = (input, expected) => () => {
			console.log('findSurroundedBy');
			const actual   = findSurroundedBy(input).string;
			expect(actual).toBe(expected, `${input} !=> ${expected}`);
		};

		const test = [
			{ input: '{{}}',                 expected: '{{}}'            },
			{ input: '{{a}}{{b}}{{c}}',      expected: '{{a}}'           },
			{ input: '{{a{{b}}{{c}}}}{{d}}', expected: '{{a{{b}}{{c}}}}' },
		];

		it('should return the {{}}',                       toBe(test[1].input, test[1].expected));
		it('{{a}}{{b}}{{c}} returns {{a}}',                toBe(test[1].input, test[1].expected));
		it('{{a{{b}}{{c}}}}{{d}} returns {{a{{b}}{{c}}}}', toBe(test[1].input, test[1].expected));
	});
	describe('.splitWithSkips()', () => {
		const toBe = ({ string, separator, regex, expected }) => () => {
			const actual = splitWithSkips(string, separator, regex);
			expect(actual).toEqual(expected);
		};

		const separator = '|';
		const regex     = /{{.*?}}/dg;
		const test      = [
			{ in: 'foo|bar',            exp: ['foo', 'bar'] },
			{ in: 'foo|{{bar}}',        exp: ['foo', '{{bar}}'] },
			{ in: 'foo|{{b|ar}}',       exp: ['foo', '{{b|ar}}'] },
			{ in: 'fo|{{o}}|{{b|a|r}}', exp: ['fo', '{{o}}', '{{b|a|r}}'] },
		];
		const args = (input) => ({ string: input.in, separator, regex, expected: input.exp  });

		it(`${test[0].in} => ${test[0].exp}`, toBe(args(test[0])));
		it(`${test[1].in} => ${test[1].exp}`, toBe(args(test[1])));
		it(`${test[2].in} => ${test[2].exp}`, toBe(args(test[2])));
		it(`${test[2].in} => ${test[2].exp}`, toBe(args(test[3])));
	});
	describe('.replacementChar()', () => {});
});

/* eslint-disable max-len */
// describe('.CSVToObject()', () => {
// 	it('test', () => {
// 		const csv = 	`
// 			combo,position,damage,worksOn,difficulty,tensionGain,notes,recipePC,video,checkedVersion
// 			(66) {{clr|1|5P}}/{{clr|1|2P}}/{{clr|2|5K}}/{{clr|2|2K}} > {{clr|1|6P}} (> {{clr|2|214K}}) ,Anywhere ,70 (73) [76] ,Everyone ,[1] {{clr|1|Very Easy}} ,,"Close range {{clr|1|P}}/{{clr|2|K}} confirm. {{clr|1|6P}} > {{clr|2|214K}} is guaranteed in the corner, but very close range or dash momentum is needed midscreen for the combo to connect midscreen. Damage given for {{clr|1|5P}} starter. ",52491(PC) 35854(PS),,1.19
// 			"{{clr|2|5K}} > {{clr|1|6P}} > {{clr|2|214K}}, {{clr|2|5K}} > {{clr|4|6H}} > {{clr|3|623S}} WS {{clr|5|5[D]}} WB ",Corner ,166,Everyone ,Medium ,28,Requires the enemy to be close to the corner for {{clr|3|623S}} not to whiff after {{clr|4|6H}} ,,,1.29
// 			��,"�,",",�,",",,,",�,"��,",,,,
// 		`;
// 		const csvObject = Util.CSVToObject(csv);
// 		csvObject.forEach((value) => console.log(value));
// 	});
// });
/* eslint-enable max-len */
