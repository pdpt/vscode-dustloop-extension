# __tests__

All tests that require vscode to run should be in the vscodeTests folder.
All other tests go in noVSCodeTests.

If a function doesn't explicitly need vscode,
try to write the file in a way that doesn't import it.

todo: figure out mocks to use for vscode testing?
