/* eslint-disable max-len */
/* eslint-disable no-unused-vars */
const { ExTester } = require('vscode-extension-tester');
const path         = require('node:path');

const main = async function main(
	// The folder containing the Extension Manifest package.json
	// Passed to `--extensionDevelopmentPath`
	extensionDevelopmentPath = path.resolve(__dirname, '../../'),
	// The path to the extension test script
	// Passed to --extensionTestsPath
	extensionTestsPath = path.resolve(__dirname, './suite/index'),
) {
	const tester = new ExTester(undefined, 'insider', undefined);
	await tester.downloadCode();
	await tester.downloadChromeDriver();
	// await tester.installVsix({ vsixFile: path.resolve(__dirname, '..', 'vsix', '<extension-name>.vsix') });
	// await tester.setupRequirements({ installDependencies: true });
	// await tester.runTests('out/spec/ui-tests/ui-test.js', { config: 'spec/ui-tests/.mocharc-debug.js', settings: 'spec/ui-tests/settings.json' });
	await tester.runTests('src/__tests__/suite/Commands.test.js', { config: '.mocharc.js', settings: 'settings.json' });
	// X:\software\source\my-code\vscode-stuff\vscode-dustloop-extension\src\__tests__\suite\Commands.test.js
};

// eslint-disable-next-line unicorn/prefer-top-level-await
// main();
module.exports = { main };
