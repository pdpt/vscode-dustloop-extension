// const assert = require('node:assert');

// You can import and use all API from the 'vscode' module
// as well as import your extension to test it
const vscode = require('vscode');
// const myExtension = require('../extension');

describe('Extension Test Suite', () => {
	vscode.window.showInformationMessage('Start all tests.');

	it('Sample test', () => {
		expect(-1).toBe([1, 2, 3].indexOf(5));
		expect(-1).toBe([1, 2, 3].indexOf(0));
	});
});
