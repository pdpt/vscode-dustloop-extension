/**
 * @file        	\src\RegexCollection.js
 * @fileoverview
 *   a collection of regexes that are used in the extension.
 *   along with helper functions to get specific ones that are needed.
 *   the regexes are grouped by expected input:
 * * input: any           - any wikitext or substring from wikitext. can be multiline and contain comments.
 * 	* skipComments        - finds commented substrings, or starting & ending with `<!--` `-->`
 * 	* lineEndsWithEquals  - finds full lines that end with equals, does not capture the newlines.
 * 		* these are assumed to be tabber headings, and thus cannot be formatted.
 * 	* coloredWord         - any text that renders as colored, of the format `{{clr|X|Y}}`
 * 		todo: does not support `{{clr|X|Y|Game=Z}}` format. probably.
 * 	* buttonWord          - finds 'button' words that should be converted to colorWords.
 * 		* `5P`, `j.632146S`, ...
 * 	* difficultyWord      - finds 'difficulty' words that should be converted to colorWords.
 * 		* `Very Easy`, `Easy`, `Medium`, `Hard`, `Very Hard`
 * 	* buttonLetter        - finds individual letters that should probably be colored.
 *    * ` P `, ` K `, ` S `, ` H `, ` D `
 *
 * * input: coloredWord   - any text that renders as colored, of the format `{{clr|X|Y}}`
 * 	* wordFromColoredWord - returns the inner text from a coloredWord
 * 		* {{clr|KEY|TEXT}} => TEXT
 *
 * * input: buttonWord
 * 	* buttonWordToKey     - returns the appropriate key for a buttonWord
 * 		* `5P` => `P`, `j.632146S` => `S`, ...
 *
 * @see         	<<filelink>>
 *
 * @author	pdpt
 * @date  	2023-10-2
 */

class RegexCollection {
	/* eslint-disable unicorn/better-regex */
	static buttonLetter = /(?<=\W)(?<![!.1-9[cfj{]+)(?<!\|)[DHKPS](?=\W)/d;
	static buttonWord   = /(?<!{{2}[cloru]{3,6}\|.+\|)[!.1-9[cfj{]+[DHKPS]+[\]}]?(?![\]}]{1,3})/d;
	static buttonWordToKey     = /(?<=(?:!|[.1-9cfj]+){?\[?)([A-Z])(?=]?}?)/d;
	static coloredText         = /{{2}[cloru]{3,6}\|.*?\|.*?}{2}/d;
	static difficultyWord      = /(Easy)|(Very Easy)|(Medium)|(Hard)|(Very Hard)(?!\|)(?!})/d;
	static lineEndsWithEquals  = /\n(.*=\s*)\n/dgm;
	static skipComments        = /(\s*<!--.*?-->\s*)/dgm;
	static wordFromColoredWord = /(?<={{2}[cloru]{3,6}\|.*?\|)(.*?)(?=}{2})/d;
	static keyFromColoredWord  = /(?<={clr\|)(.*?)(?=\|.*?}})/d;
	/* eslint-enable unicorn/better-regex */

	/** regexes for text that shouldn't be formatted in general */
	static doNotFormat() {
		const  { skipComments, lineEndsWithEquals } = RegexCollection;
		return { skipComments, lineEndsWithEquals };
	}
	/** regexes to find text that should be skipped when adding color */
	static doNotColor() {
		const  { skipComments, lineEndsWithEquals, coloredText } = RegexCollection;
		return { skipComments, lineEndsWithEquals, coloredText };
	}
	/** regexes to find text that should be skipped when removing color */
	static doNotUncolor() {
		const  { skipComments, lineEndsWithEquals } = RegexCollection;
		return { skipComments, lineEndsWithEquals };
	}
}

module.exports = { RegexCollection };
