/**
 * @file        	\src\VSCodeHelper.js
 * @fileoverview
 * 	Helper class for interacting with vscode.
 * 	Ideally, this will be the only place that vscode is imported.
 *
 * @see <https://vscode-api.js.org/modules/vscode.html>
 * @see <https://vscode-api.js.org/interfaces/vscode.TextDocument.html>
 * @see <https://vscode-api.js.org/interfaces/vscode.TextEditor.html>
 * @see <https://vscode-api.js.org/classes/vscode.Selection.html>
 *
 * @author	pdpt
 * @date  	2023-10-1
 * @module
 * @class VSCodeHelper
 */
const vscode = require('vscode');
const { Util } = require('./utils');

const { Range } = vscode;

class VSCodeHelper {
	static getText() {
		const { window } = vscode;
		const editor = window.activeTextEditor;
		const { document, selection } = editor;

		const text = document.getText(selection) || document.getText();
		return text;
	}
	/* eslint-disable max-len */
	/**
	 * @description
	 * a command for replacing the selected text with some new text.
	 * @date 2023-10-01
	 * @static
	 * @param {function(string): string} replaceFunction	function that takes in the unmodified text, and returns text to replace it with.
	 * @memberof VSCodeHelper
	*/
	/* eslint-enable max-len */
	static replaceCommand(replaceFunction) {
		const { window } = vscode;
		const editor = window.activeTextEditor;
		const { document, selection } = editor;

		// const oldText  = selection.isEmpty ? document.getText() : document.getText(selection);
		const oldText  = VSCodeHelper.getText();
		const newText  = replaceFunction(oldText);
		const range    = new vscode.Range(document.positionAt(0), document.positionAt(oldText.length));
		const location = selection.isEmpty ? range : selection;

		return VSCodeHelper.replaceRange(location, newText);
	}
	/**
	 * replaces a range of text with a new string.
	 * @date 10/1/2023 - 12:56:24 AM
	 *
	 * @static
	 * @memberof VSCodeHelper
	 * @param {vscode.Range} location
	 * @param {string}       string
	 */
	static replaceRange(location, string) {
		const { window } = vscode;
		const editor = window.activeTextEditor;
		// const { document, selection } = editor;

		editor.edit((builder) => VSCodeHelper.#replaceCallback(builder, location, string));
	}

	static removeNotInSelection = function removeNotInSelection(allMatches, lineNumber, selection) {
		if (selection.isEmpty) {
			return allMatches;
		}
		const filteredArray = allMatches.filter((entry) => {
			console.log('allmatches.filter');
			const { index, oldText } = entry;
			const end = index + oldText.length;

			// check the intersection of the selection & the current match
			const range = new Range(lineNumber, index, lineNumber, end);
			const intersection = selection.intersection(range) || selection.contains(range);
			const didIntersect = intersection ? !intersection.isEmpty : false;

			// only keep those that did intersect. a word partially highlighted is considered in the selection.
			return didIntersect;
		});
		return filteredArray;
	};

	/**
	 * Description placeholder
	 * @date 10/1/2023 - 1:05:59 AM
	 *
	 * @static
	 * @param {vscode.TextEditorEdit} editBuilder
	 * @param {vscode.Range} location
	 * @param {string} value
	 */
	static #replaceCallback(editBuilder, location, value) {
		editBuilder.replace(location, value);
	}

	static getRange(document) {
		console.log('getrange');
		const { lineNumber, index, oldText } = this;

		const line     = document.lineAt(lineNumber);
		const { text } = line;

		const start = text.indexOf(oldText, index);
		const end   = start + oldText.length;

		const range = new Range(lineNumber, start, lineNumber, end);
		return range;
	}

	/**
	 * @description
	 *
	 * @todo clean this up @ probably remove it.
	 * @date 2023-10-03
	 * @param {*} getNewText					function that takes a string,
	 *																and returns the string it should be replaced with.
	 * @param {*} findOldTextRegex		regex that finds text that needs to be replaced in a line of arbitrary text.
	 *
	 * @memberof VSCodeHelper
	 */
	static convertText(getNewText, findOldTextRegex, skipLines) {
		const { window } = vscode;
		const editor = window.activeTextEditor;
		const { document, selection } = editor;
		Util.assert(editor, 'Editor is not opening');

		// eslint-disable-next-line max-len
		const getLineArray = (document_, lineNumber) => {
			const array = Util.getLineMatches(
				document_,
				lineNumber,
				findOldTextRegex,
				skipLines,
				selection,
			);
			Util.isArray(array);
			return array;
		};

		const editArray = Util.getAllEdits(document, getLineArray, getNewText, selection);
		Util.isArray(editArray);
		if (editArray.length <= 0) {
			console.warn('found no matches');
		}
		const uniqueArray = Util.removeDupes(editArray, EditObject.compare);

		console.log(`found ${editArray.length} matches, ${uniqueArray.length} unique`);

		Util.isArray(uniqueArray, 'convertText uniqueArray undefined');
		Util.warn(uniqueArray.length <= 0, 'convertText uniqueArray empty');

		editor.edit((editBuilder) => VSCodeHelper.editCallback(editBuilder, document, uniqueArray));
	}

	/**
	* callback function for the editor.edit function
	* @date 9/22/2023 - 1:39:10 AM
	*
	* @param {*} editBuilder
	* @param {*} document
	* @param {*} array
	*/
	// editBuilder, document, uniqueArray
	static editCallback = function editCallback(editBuilder, document, array) {
		Util.assert(editBuilder);
		Util.isArray(array);
		Util.warn(array.length <= 0, 'editCallback empty array');

		array.forEach((editItem) => {
			console.log('editcallback foreach');
			const { lineNumber, oldText, newText, index } = editItem;

			// let start = text.indexOf(oldText, index) + index;
			const start = index;
			const end = start + oldText.length;

			if (start !== -1) {
				const range = new Range(lineNumber, start, lineNumber, end);
				editBuilder.replace(range, newText);
			}
		});
	};

	/**
	 * @description creates a new file, and puts the content of string in that file.
	 * @date 2023-10-06
	 * @static
	 * @param {*} string
	 * @memberof VSCodeHelper
	 */
	static stringInNewFile(string) {
		// create the document
		const options     = { language: 'wikitext', content: string };
		const newDocument = vscode.workspace.openTextDocument(options);
		// focus on the document
		vscode.window.showTextDocument(newDocument);
		return newDocument;
	}
}

module.exports = { VSCodeHelper };
