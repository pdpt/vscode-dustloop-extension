/**
 * @file        	\src\utils.js
 * @fileoverview
 *   various helper functions, that are used in multiple places, or do not fit with a specific class.
 *
 * @author	pdpt
 * @date  	2023-10-3
 */

class Util {
	static ifMessage(assertion, messageFunction = console.log, ...messages) {
		if (!assertion) {
			messageFunction(messages.join?.(',') || messages);
			return false;
		}
		return true;
	}

	static assert(truth, ...messages) {
		const messageFunction = (string) => {
			console.groupCollapsed('assertion trace');
			console.trace();
			console.groupEnd();
			throw new Error(string);
		};
		return Util.ifMessage(truth, messageFunction, messages);
	}

	static warn(truth, ...messages) {
		const messageFunction = (string) => {
			const warning = console.warn(string);
			console.groupCollapsed('warning trace');
			console.trace();
			console.groupEnd();
			return warning;
		};
		return Util.ifMessage(truth, messageFunction, messages);
	}

	static assertType(first, second, third) {
	// eslint-disable-next-line valid-typeof
		const check = (typeof first === second);
		const assertMessage   = `${third}: typeof ${first} must be ${second}`;
		Util.assert(check, assertMessage);
	}

	static isArray(array, ...messages) { Util.assert(Array.isArray(array), messages); }

	/**
	 * @description
	 *   returns the indexes of the first match of a regex
	 *
	 * @date 2023-10-03
	 * @param   {string}   string the string to split
	 * @param   {RegExp}   regex  the regex to match against
	 * @returns {[number]}        an array formatted at [matchStart, matchEnd].
	 */
	static getRegexIndexes(string, regex) {
		let match;
		switch (true) {
		case (regex instanceof RegExp): {
			match = regex.exec(string)?.indices[0];
			break;
		}
		case (Array.isArray(regex)): {
			match = regex.reduce(((previous, current) => {
				Util.assert(current.flags.includes('d'), 'regexp is missing "d" flag');
				return 	previous || current.exec(string)?.indices[0];
			}), false);
			break;
		}
		case (typeof regex === 'object'): {
			match = Object.values(regex).reduce(((previous, current) => {
				Util.assert(current.flags.includes('d'), 'regexp is missing "d" flag');
				return 	previous || current.exec(string)?.indices[0];
			}), false);
			break;
		}
		case (typeof regex === 'string'): {
			// eslint-disable-next-line security/detect-non-literal-regexp
			match = new RegExp(regex, 'd').exec(string)?.indices[0];
			break;
		}
		default: {
			Util.warn(false, 'invalid regex: ', regex);
			match = false;
			break;
		}
		}

		// convert falsey values to false
		return match || false;
	}

	/**
	 * @description
	 *   takes a string & a regex, returns an array of 3 strings,
	 *   cutting at each end of the regex match.
	 *
	 * @date 2023-10-03
	 * @param   {string}  string the string to split
	 * @param   {RegExp}  regex  the regex to match against
	 * @returns {[string]}       the split string as an array
	 */
	static getSplitString(string, regex) {
		const indexes = Util.getRegexIndexes(string, regex);
		if (!indexes) {
			return false;
		}

		const pre   = string.slice(0, indexes[0]);
		const match = string.slice(indexes[0], indexes[1]);
		const post  = string.slice(indexes[1]);

		return [pre, match, post];
	}

	static isBetween(valueA, valueB, valueC) {
		if (valueA < valueB && valueB < valueC) return true;
		if (valueC < valueB && valueB < valueA) return true;
		return false;
	}

	/**
	 * returns formatted object of regex matches with the given string
	 * @date 9/23/2023 - 5:45:25 PM
	 *
	 * @param {String} string
	 * @param {RegEx} regex
	 *
	 * @returns [{oldText: String, index: Number}]
	 */
	static getMatchesFromString(string, regex) {
		const execArray = [];
		let result = regex.exec(string);
		while ((result) !== null) {
			execArray.push(result);
			result = regex.exec(string);
		}

		const mappedArray = execArray.map((value) => {
			const mapObject = { oldText: value[0], index: value.index };

			return mapObject;
		});

		return mappedArray || [];
	}

	// eslint-disable-next-line id-length
	static removeDupes(array, compareFunction = (a, b) => (a === b)) {
		const unique = array.filter((itemA, indexA, arrayA) => {
			// compare to every other item
			const foundIndex = arrayA.findIndex((itemB, indexB) => {
				if (indexA === indexB) {
					return false;
				}

				// compare via compareFunction
				const areEqual = compareFunction(itemA, itemB);
				return areEqual;
			});

			if (foundIndex !== -1 && foundIndex !== indexA) {
				Util.warn(true, 'found a duplicate');
				return false;
			}
			return true;
		});
		Util.isArray(unique);
		return unique;
	}

	// static getEditObject(item, lineNumber, getNewText) {
	// 	const { index, oldText } = item;
	// 	const newText = getNewText(oldText);

	// 	Util.assert(typeof    item === 'object', 'getEditObject    item');
	// 	Util.assert(typeof   index === 'number', 'getEditObject   index');
	// 	Util.assert(typeof newText === 'string', 'getEditObject newText');

	// 	const returnObject = new EditObject(lineNumber, index, oldText, newText);

	// 	return returnObject;
	// }

	// static getAllEdits(document, getLineArray, getNewText, selection) {
	// 	Util.assert(document);
	// 	Util.assert(getLineArray);
	// 	Util.assert(getNewText);

	// 	const { start, end } = selection;
	// 	const { line: startLine } = start;
	// 	const { line: endLine } = end;

	// 	const editArray = [];
	// 	const endIndex  = endLine || document.lineCount;

	// 	for (
	// 		let lineNumber = startLine || 0;
	// 		(lineNumber < endIndex);
	// 		lineNumber += 1
	// 	) {
	// 		// todo: skipline check goes here!
	// 		const array = getLineArray(document, lineNumber);
	// 		array.forEach((item) => {
	// 			const editObject = getEditObject(item, lineNumber, getNewText);
	// 			if (editObject) {
	// 				editArray.push(editObject);
	// 			}
	// 		});
	// 	}

	// 	Util.warn(editArray.length <= 0, 'getAllEdits editArray empty');
	// 	Util.isArray(editArray);

	// 	return editArray;
	// }

	/**
	 * returns array from scanning a line of text with matchall.
	 * @date 9/22/2023 - 1:29:25 AM
	 *
	 * @param {*} document
	 * @param {*} lineNumber
	 * @param {*} regex
	 * @returns {*}
	 */
	static getLineMatches(document, lineNumber, regex, skipLines, selection) {
		console.log('this is the function to debug');
		Util.assertType(document,   'object');
		Util.assertType(lineNumber, 'number');
		Util.assertType(regex,      'object');
		Util.assertType(skipLines,  'object');
		Util.assertType(selection,  'object');

		if (Array.isArray(regex)) {
			let  returnArray = [];

			regex.forEach((item) => {
				returnArray = [
					...returnArray,
					...Util.getLineMatches(document, lineNumber, item, skipLines, selection),
				];
			});
			if (!returnArray) {
				console.log('wrf');
			}
			return returnArray;
		}

		const line = document.lineAt(lineNumber);
		const { text, range } = line;

		// if there is no selection, everything is in the selection.
		const { isEmpty } = selection;
		// otherwise, if there is some overlap between the line & the selection, it's in the selection.
		const intersection = selection.intersection(range);
		// const contains     = range.contains(selection);
		// if one is within the other.
		const contains     = selection.contains(range) || range.contains(selection);
		const didIntersect = intersection ? !intersection.isEmpty : false;
		const inSelection  = isEmpty || didIntersect || contains;
		const inSkipLines  = !!(skipLines.some((value) => value(text)));
		const shouldSkip   = inSkipLines || !inSelection;

		if (shouldSkip) {
			return [];
		}

		// formated as [{oldText: String, index: Number}]s
		const allMatches = Util.getMatchesFromString(text, regex);

		const selectedMatches = Util.removeNotInSelection(allMatches, lineNumber, selection);

		return selectedMatches;
	}

	// eslint-disable-next-line max-len
	// from: <https://www.bennadel.com/blog/1504-ask-ben-parsing-csv-strings-with-javascript-exec-regular-expression-command.htm>

	// This will parse a delimited string into an array of
	// arrays. The default delimiter is the comma, but this
	// can be overriden in the second argument.
	static CSVToArray(stringData, fieldDelimiter = ',') {
		// Check to see if the delimiter is defined. If not,
		// then default to comma.
		// fieldDelimiter = (fieldDelimiter || ',');

		// Create a regular expression to parse the CSV values.
		// eslint-disable-next-line security/detect-non-literal-regexp
		const objectPattern = new RegExp(
		// Delimiters.
		// Quoted fields.
		// Standard fields.
			(`
		(\\${fieldDelimiter}|\\r?\\n|\\r|^)
		(?:"([^"]*(?:""[^"]*)*)"|
		([^"\\${fieldDelimiter}\\r\\n]*))
		`.replaceAll(/\s/gim, '')),
			'gi',
		);

		// Create an array to hold our data. Give the array
		// a default empty first row.
		const arrayData = [[]];

		// Create an array to hold our individual pattern
		// matching groups.
		let arrayMatches;
		let stringMatchedValue;

		// Keep looping over the regular expression matches
		// until we can no longer find a match.
		while ((arrayMatches = objectPattern.exec(stringData))) {
		// Get the delimiter that was found.
			const stringMatchedDelimiter = arrayMatches[1];

			// Check to see if the given delimiter has a length
			// (is not the start of string) and if it matches
			// field delimiter. If id does not, then we know
			// that this delimiter is a row delimiter.
			if (
				stringMatchedDelimiter.length > 0
				&& (stringMatchedDelimiter !== fieldDelimiter)
			) {
			// Since we have reached a new row of data,
			// add an empty row to our data array.
				arrayData.push([]);
			}

			stringMatchedValue = arrayMatches[2]?.replaceAll('""', '"') || arrayMatches[3];
			// Now that we have our delimiter out of the way,
			// let's check to see which kind of value we
			// captured (quoted or unquoted).
			// if (arrayMatches[2]) {
			// 	// We found a quoted value. When we capture
			// 	// this value, unescape any double quotes.
			// 	stringMatchedValue = arrayMatches[2].replaceAll(
			// 		'""',
			// 		'"',
			// 	);
			// } else {
			// 	// We found a non-quoted value.
			// 	stringMatchedValue = arrayMatches[3];
			// }

			// Now that we have our value string, let's add
			// it to the data array.
			arrayData.at(-1).push(stringMatchedValue);
		}

		// Return the parsed data.
		return (arrayData);
	}

	/**
	 * takes in a string that is CSV format,
	 * returns an array of objects where the keys are column names & the values are cells.
	 * @date 9/26/2023 - 5:35:24 PM
	 *
	 * @param {string} stringData string in CSV format.
	 * @param {string} stringDelimiter	cell separator, usually `,`
	 * @returns {*} Array[row{}] array of row objects, keys are header column names, vals are cells of that row.
	 */
	static CSVToObject(stringData, stringDelimiter = ',') {
		const trimData = stringData.trim();
		const array = Util.CSVToArray(trimData, stringDelimiter);

		const header = array.shift();
		const objectArray = array.map((value) => {
			const object = {};
			header.forEach((headerValue, index) => {
				const trimHeader = headerValue.trim();
				// eslint-disable-next-line security/detect-object-injection
				const trimValue = value[index].trim();
				// eslint-disable-next-line security/detect-object-injection
				object[trimHeader] = trimValue;
			});
			return object;
		});

		return objectArray;
	}

	/**
	 * {{
	 * 	{{
	 * 		{{
	 *
	 * 		}}
	 * 	}}
	 * }}
	 * @date 9/27/2023 - 10:51:03 PM
	 *
	 * @param {string} string
	 * @param {*} stringStart
	 * @returns {*}
	 */
	/**
	 * @description
	 *   finds a string at the appropriate "block level".
	 * 	* if we are looking for a string surrounded by '[', ']'
	 *  * return the string starting & ending with '[', ']' ...
	 *  * but skip over any inner pairs
	 *
	 * @example
	 *   '[a][b][c]'    returns '[a]'
	 *   '[a[b][c]][d]' returns '[a[b][c]]'
	 *
	 * @date 2023-10-05
	 * @static
	 * @param  {string} string
	 * @param  {number} [stringStart=0]
	 * @param  {number} [stringEnd=string.length]
	 * @param  {string} [prefix='{{']
	 * @param  {string} [postfix='}}']
	 *
	 * @return {*}
	 * @memberof Util
	 */
	static findSurroundedBy(
		string,
		stringStart = 0,
		stringEnd   = string.length,
		prefix      = '{{',
		postfix     = '}}',
	) {
		// const x = [[[[], []], [[[]]]]];
		// our return value will start at the first match.
		const start = string.indexOf(prefix, stringStart);
		// let index = start + prefix.length;
		let index = stringStart;
		let depth = 0;
		do {
			const nextPre  = string.indexOf(prefix, index);
			const nextPost = string.indexOf(postfix, index);
			if (nextPost < nextPre || nextPre === -1) {
				depth -= 1;
				index = nextPost + prefix.length;
			} else {
				depth += 1;
				index = nextPre + postfix.length;
			}
		} while (depth !== 0);

		return { start, end: index, string: string.slice(start, index) };
		// if we find prefix,  depth + 1
		// if we find postfix, depth - 1
		// when depth = 0, we are done

		// find the next instance of `{{`&`}}`
		// const firstStart = string.indexOf(prefix, stringStart + prefix.length);
		// let   nextStart  = firstStart;
		// let   nextEnd    = string.indexOf(postfix, stringStart) + postfix.length;

		// const prefixMatchStart = string.indexOf(prefix, stringStart);
		// const prefixMatchEnd   = prefixMatchStart + prefix.length;

		// const postfixMatchStart = string.indexOf(postfix, stringStart);
		// const postfixMatchEnd   = postfixMatchStart + postfix.length;

		// const nextPrefixStart  = string.indexOf(prefix, prefixMatchEnd);
		// const nextPostfixStart = string.indexOf(postfix, postfixMatchEnd);

		// // if we don't find anymore prefix matches, return the string.
		// if (nextPrefixStart < 0) {
		// 	const returnString = string.slice(prefixMatchStart, postfixMatchEnd);
		// 	return { start: prefixMatchStart, end: postfixMatchEnd, string: returnString };
		// }

		// let nextStart = prefixMatchEnd;
		// let nextEnd   = postfixMatchStart;
		// // if there is a start before our end, skip over the next pair;
		// while (nextPrefixStart < nextPostfixStart) {
		// 	const innerMatch = Util.findSurroundedBy(string, nextStart, nextEnd, prefix, postfix);

		// 	nextStart = Util.findSurroundedBy(string, nextStart, stringEnd, prefix, postfix).end;
		// 	nextEnd   = string.indexOf(postfix, nextStart + postfix.length);
		// }

		// const x = {
		// 	start  : firstStart,
		// 	end    : nextStart,
		// 	string : string.slice(firstStart, nextStart),
		// };
		// console.log(x);
		// return x;
	}

	/**
	 * @description
	 *   same as String.split(),
	 *   but skips over separators inside a skip regex.
	 * @date 2023-10-04
	 * @static
	 * @param  {string|regex} separator
	 * @param  {string|regex} skipRegex
	 * returns {[string]}
	 * @memberof Util
	 */
	static splitWithSkips(string, separator, skipRegex) {
		Util.assert(skipRegex.flags.includes('g'), 'splitWithSkips passed non-global regex');
		// array of things to skip
		const skips = [...string.matchAll(skipRegex)].map((value) => value[0]);
		const { replacementChar } = Util;

		// a string where all skips are replaced with Util.replacementChar
		// let skippedString = '';
		// skips.forEach((skip) => {
		// 	skippedString = string.replace(skip, replacementChar);
		// });
		const skippedString = string.split(skipRegex).join(replacementChar);

		// now split the string as normal
		const split = skippedString.split(separator);

		// and replace the character with the original skipped string.
		skips.forEach((skip) => {
			/* eslint-disable security/detect-object-injection */
			// find the next replacementChar
			const matchIndex = split.findIndex((subSplit) => subSplit.includes(replacementChar));
			const match = split[matchIndex];
			// replace the character with the skipstring
			split[matchIndex] = match?.replace(replacementChar, skip);
			/* eslint-enable security/detect-object-injection */
		});

		// return the split array.
		return split;
	}

	static replacementChar = '�';
}

// class EditObject {
// 	constructor(lineNumber, index, oldText, newText) {
// 		assertType(lineNumber, 'number');
// 		assertType(index, 'number');
// 		assertType(oldText, 'string');
// 		assertType(newText, 'string');

// 		this.lineNumber = lineNumber;
// 		this.index      = index;
// 		this.oldText    = oldText;
// 		this.newText    = newText;
// 	}

// 	static compare(first, second) {
// 		if (first.lineNumber !== second.lineNumber) return false;
// 		if (first.index      !== second.index) return false;
// 		console.log('line & index match');
// 		if (first.oldText    !== second.oldText) return false;
// 		if (first.newText    !== second.newText) return false;
// 		console.log('match found');
// 		return true;
// 	}

// 	static intersection(first, second) {
// 		if (first.lineNumber !== second.lineNumber) return false;
// 		const aStart = first.index;
// 		const bStart = second.index;
// 		const aEnd   = first.index  + first.oldText.length;
// 		const bEnd   = second.index + second.oldText.length;

// 		const bStartBetweenA = isBetween(aStart, bStart, aEnd);
// 		const bEndBetweenA   = isBetween(aStart, bEnd, aEnd);
// 		const aStartBetweenB = isBetween(bStart, aStart, bEnd);
// 		const aEndBetweenB   = isBetween(bStart, aEnd, bEnd);

// 		if (bStartBetweenA || bEndBetweenA || aStartBetweenB || aEndBetweenB) {
// 			console.log('intersection!');
// 			return true;
// 		}
// 		return false;
// 	}

// 	toString() {
// 		return `{l: ${this.lineNumber}, i: ${this.index}, o: ${this.oldText}, n: ${this.newText}}`;
// 	}
// }

module.exports = {
	Util,
};
