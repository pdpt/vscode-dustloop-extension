// convertColorKeyWords.js;

const COLOR_ARRAY = [
	[1,	'P',  '#d96aca', 'Very Easy'],
	[2,	'K',  '#1ba6ff', 'Easy'],
	[3,	'S',  '#16df53', 'Medium'],
	[4,	'H',  '#ff0000', 'Hard'],
	[5,	'D',  '#e8982c', 'Very Hard'],
];
const NUMBER_INDEX     = 0;
const BUTTON_INDEX     = 1;
const HEX_INDEX        = 2;
const DIFFICULTY_INDEX = 3;

const convert = function convert(input, type) {
	const returnValue = COLOR_ARRAY.find((color) => color.includes(input));

	// eslint-disable-next-line security/detect-object-injection
	return returnValue?.[type] || console.warn('color not found!');
};

/* eslint-disable unicorn/prevent-abbreviations */
const conToNum  = function convertToNumber(input)     { return convert(input, NUMBER_INDEX); };
const conToBtn  = function convertToButton(input)     { return convert(input, BUTTON_INDEX); };
const conToHex  = function convertToHex(input)        { return convert(input, HEX_INDEX); };
const conToDiff = function convertToDifficulty(input) { return convert(input, DIFFICULTY_INDEX); };
/* eslint-enable unicorn/prevent-abbreviations */

module.exports = {
	convertToNumber     : conToNum,
	convertToButton     : conToBtn,
	convertToHex        : conToHex,
	convertToDifficulty : conToDiff,
};
