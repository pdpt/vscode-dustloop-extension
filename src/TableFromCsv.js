// TableFromCSV.js
const {
	convertText,
	assert,
	CSVToObject,
} = require('./utils');

/**
 * example data:
 *  a,  b,   c,     d
 *  �,",","�,","��,,"
 * |"| , | ", | "",, |
 *
 * `�` === \uFFFD
 * see: <https://unicodeplus.com/U+FFFD>
 */
const tableFromCsv = function tableFromCsv(
	Csv,
	fieldDelim = ',',
	stringDelim = '"',
	replacementChar = '�',
) {
	console.log(convertText);
	console.log(assert);
	const tableObject = CSVToObject(Csv, fieldDelim);
	console.log(tableObject);
	console.log(Csv, fieldDelim, stringDelim, replacementChar);
};

module.exports = { tableFromCsv };
