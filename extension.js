// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
const vscode = require('vscode');

const { registerCommand } = vscode.commands;

const { commentDown } = require('./src/commentDown');
const { Commands }    = require('./src/Commands');
// This method is called when your extension is activated
// Your extension is activated the very first time the command is executed

/**
 * @param {vscode.ExtensionContext} context
 */
const activate = function activate(context) {
	// Use the console to output diagnostic information (console.log) and errors (console.error)
	// This line of code will only be executed once when your extension is activated
	console.log('Congratulations, your extension "dustloop" is now active!');
	console.log(Commands.addColor.name);

	// The command has been defined in the package.json file
	// eslint-disable-next-line import/extensions, global-require
	console.log(require('./package.json'));

	// The commandId parameter must match the command field in package.json
	const addColor           = registerCommand('dustloop.addColor',      Commands.addColor);
	const removeColor        = registerCommand('dustloop.removeColor',   Commands.removeColor);
	const colorToHex         = registerCommand('dustloop.colorToHex',    Commands.colorToHex);
	const colorToButton      = registerCommand('dustloop.colorToButton', Commands.colorToButton);
	const generateComboTable = registerCommand('dustloop.generateComboTable', Commands.generateComboTable);

	context.subscriptions.push(addColor, removeColor, colorToHex, colorToButton, generateComboTable);
};

// This method is called when your extension is deactivated
const deactivate = function deactivate() {};

module.exports = {
	activate,
	deactivate,
};
