module.exports = {
  timeout: '1500',
  'inline-diffs': true,
  watch: true,
  spec: ['./src/__tests__/noVSCodeTests/**/*.test.js'],
  parallel: true,
  color: true,
};