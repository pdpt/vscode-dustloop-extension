/* eslint-disable import/no-unresolved */
/* eslint-disable unicorn/prefer-module */
/**
 * main.js
 *
 * contains code for adding scripts contained in other files.
 * any command you want to run should be imported here.
 */
const vscode = require('vscode');

const { TheoryBox } = require('./src/dustloop/wikiObjects/TheoryBox');

const { readCombos } = require('./src/dustloop/readCombos');

const { commentDown } = require('./src/commentDown');
const {
	addColorFormatting, convertColorToHex, convertHexToColor, removeColorFormatting,
} = require('./src/dustloop/addColorFormatting');
const { MacroCommands } = require('./src/MacroCommands');
const { GGSTComboTableRow } = require('./src/wikiObjects/GGSTComboTableRow');

console.log(GGSTComboTableRow);
console.log(TheoryBox);

const getSelection = function getSelection() {
	const { selections, selection } = vscode.window.activeTextEditor;
	console.clear();
	console.groupCollapsed('selection');
	console.log(selections);
	console.log(`start: ${selection.start.line}, ${selection.start.character}`);
	console.log(`end: ${selection.end.line}, ${selection.end.character}`);
	console.log(`isEmpty: ${selection.isEmpty}`);
	console.log(`isSingleLine: ${selection.isSingleLine}`);
	console.log(`anchor: ${selection.anchor.line}, ${selection.anchor.character}`);
	console.log(`active: ${selection.active.line}, ${selection.active.character}`);
	console.log(`isReversed: ${selection.isReversed}`);

	console.groupEnd();
	return selection;
};

// every command imported should be added
MacroCommands.add(
	readCombos,
	addColorFormatting,
	removeColorFormatting,
	convertColorToHex,
	convertHexToColor,
	getSelection,
	commentDown,
);

module.exports.macroCommands = MacroCommands.getObject();
