// eslint-disable-next-line import/extensions, no-unused-vars
const x = require('../../.eslintrc.js');

module.exports = {
	ignorePatterns: ['exampleFiles', 'test-resources', 'coverage'],

	root : true,
	env  : {
		browser  : false,
		commonjs : true,
		es6      : true,
		node     : true,
		mocha    : true,
	},
	parser        : '@babel/eslint-parser',
	parserOptions : {
		ecmaVersion  : 2018,
		ecmaFeatures : {
			jsx: true,
		},
		sourceType: 'module',
	},
	extends: [
		// 'eslint:recommended',
		// 'plugin:import/recommended',
		// 'plugin:import/electron',
		// 'plugin:import/typescript',
		// 'plugin:jsx-a11y/recommended',
		// 'airbnb',
		// https://github.com/airbnb/javascript
		'X:\\software\\source\\my-code\\.eslintrc.js',
	],
	settings: {
		'import/core-modules': ['vscode'],
	},
	rules: {
		'no-const-assign'                  : 'warn',
		'no-this-before-super'             : 'warn',
		'no-undef'                         : 'warn',
		'no-unreachable'                   : 'warn',
		'no-unused-vars'                   : 'warn',
		'constructor-super'                : 'warn',
		'valid-typeof'                     : 'warn',
		'unicorn/prefer-module'            : 0,
		'unicorn/numeric-separators-style' : [1, { onlyIfContainsSeparator: true }],
		'unicorn/filename-case'            : [
			'error',
			{
				cases: {
					camelCase  : true,
					pascalCase : true,
				},
				ignore: [
					'.*GGST.*',
					'.*VSCode.*',
				],
			},
		],
		'unicorn/prevent-abbreviations': [
			'warn',
			{
				allowList: {
					args: true,
				},
			},
		],

	},
};
